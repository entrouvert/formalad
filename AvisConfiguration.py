# -*- coding: utf-8 -*-
#
# File: AvisConfiguration.py
#
# Copyright (c) 2007 by []
# Generator: ArchGenXML Version 1.5.3 dev/svn
#            http://plone.org/products/archgenxml
#
# GNU General Public License (GPL)
#
# This program is free software; you can redistribute it and/or
# modify it under the terms of the GNU General Public License
# as published by the Free Software Foundation; either version 2
# of the License, or (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program; if not, write to the Free Software
# Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA
# 02110-1301, USA.
#

__author__ = """unknown <unknown>"""
__docformat__ = 'plaintext'

from AccessControl import ClassSecurityInfo
from Products.Archetypes.atapi import *
from config import *


from Products.CMFCore.utils import UniqueObject

    
##code-section module-header #fill in your manual code here
from Products.CMFCore.utils import getToolByName
##/code-section module-header

schema = Schema((

),
)

##code-section after-local-schema #fill in your manual code here
##/code-section after-local-schema

AvisConfiguration_schema = BaseSchema.copy() + \
    schema.copy()

##code-section after-schema #fill in your manual code here
##/code-section after-schema

class AvisConfiguration(UniqueObject, BaseContent):
    """
    """
    security = ClassSecurityInfo()
    __implements__ = (getattr(UniqueObject,'__implements__',()),) + (getattr(BaseContent,'__implements__',()),)

    # This name appears in the 'add' box
    archetype_name = 'AvisConfiguration'

    meta_type = 'AvisConfiguration'
    portal_type = 'AvisConfiguration'
    allowed_content_types = []
    filter_content_types = 0
    global_allow = 0
    #content_icon = 'AvisConfiguration.gif'
    immediate_view = 'base_view'
    default_view = 'base_view'
    suppl_views = ()
    typeDescription = "AvisConfiguration"
    typeDescMsgId = 'description_edit_avisconfiguration'
    #toolicon = 'AvisConfiguration.gif'

    _at_rename_after_creation = True

    schema = AvisConfiguration_schema

    ##code-section class-header #fill in your manual code here
    ##/code-section class-header


    # tool-constructors have no id argument, the id is fixed
    def __init__(self, id=None):
        BaseContent.__init__(self,'portal_avisconfiguration')
        self.setTitle('AvisConfiguration')
        
        ##code-section constructor-footer #fill in your manual code here
        ##/code-section constructor-footer


    # tool should not appear in portal_catalog
    def at_post_edit_script(self):
        self.unindexObject()
        
        ##code-section post-edit-method-footer #fill in your manual code here
        ##/code-section post-edit-method-footer


    # Methods

    security.declarePublic('getAvisFolder')
    def getAvisFolder(self):
      '''Checks the existence of the avis folder in the user's home folder
         (create it if not) and returns the path to this folder.'''
      portal = getToolByName(self, 'portal_url').getPortalObject()
      home_folder = portal.portal_membership.getHomeFolder()
      if home_folder is None: # Necessary for the admin zope user
        return ''
      if not hasattr(home_folder, AVIS_FOLDER):
        # Create the Req folder into the user's home folder
        id = home_folder.invokeFactory(type_name="AvisFolder", id=AVIS_FOLDER,
                                       title="Mes avis")
        avisFolder = getattr(home_folder, id)
        avisFolder.description = "C'est dans ce dossier que vous pouvez creer "\
                                 "vos avis."
        avisFolder.reindexObject()
      else:
        avisFolder = getattr(home_folder, AVIS_FOLDER)
      return avisFolder.absolute_url()


registerType(AvisConfiguration, PROJECTNAME)
# end of class AvisConfiguration

##code-section module-footer #fill in your manual code here
##/code-section module-footer



