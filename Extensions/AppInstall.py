# -*- coding: utf-8 -*-
# ------------------------------------------------------------------------------
# Copyright (c) 2007 by CommunesPlone
#
# GNU General Public License (GPL)
#
# This program is free software; you can redistribute it and/or
# modify it under the terms of the GNU General Public License
# as published by the Free Software Foundation; either version 2
# of the License, or (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program; if not, write to the Free Software
# Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA
# 02110-1301, USA.
#
__author__ = """Gaetan Delannay <gde at easi.wallonie.be>"""
__docformat__ = 'plaintext'

# ------------------------------------------------------------------------------
from Products.Avis.config import *

# ------------------------------------------------------------------------------
def install(self):
    # Create the folder 'Avis' at the rot of the site.
    if not hasattr(self, 'avis'):
        self.invokeFactory('AvisFolder', id='avis', title='Avis')
    # Add a tab in portal_actions
    actions = self.portal_actions.listActions()
    action_ids = [action.id for action in actions]
    if False and not TAB_ID in action_ids:
        self.portal_actions.addAction(
          TAB_ID, 'Avis', 'string:$portal_url/avis', 'member',
          'View', 'portal_tabs')

    # Add OpenOffice icon
    if not self.portal_actionicons.queryActionIcon('plone', 'asOdt'):
        self.portal_actionicons.addActionIcon('plone', 'asOdt', 'odt.png',
                                              title='odt')
    # Add Word icon
    if not self.portal_actionicons.queryActionIcon('plone', 'asDoc'):
        self.portal_actionicons.addActionIcon('plone', 'asDoc', 'doc.png',
                                              title='doc')
    return True
# ------------------------------------------------------------------------------
