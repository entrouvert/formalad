# -*- coding: utf-8 -*-
##
# GNU General Public License (GPL)
#
# This program is free software; you can redistribute it and/or
# modify it under the terms of the GNU General Public License
# as published by the Free Software Foundation; either version 2
# of the License, or (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program; if not, write to the Free Software
# Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA
# 02110-1301, USA.
#

__author__ = """Fredric Peters <fpeters@entrouvert.com>"""
__docformat__ = 'plaintext'

import os
import time
import appy.pod.renderer
from DateTime import DateTime

from Products.Archetypes.atapi import *

from config import *

class AvisOdt:
    '''Base class for common methods of Avis'''

    def _generate(self, response, fileType):
        '''Generates a document that represents this advice.
        The document format is specified by p_fileType.'''
        # First, generate the PDF in a temp file
        tempFileName = '/tmp/%s.%f.%s' % (self._at_uid, time.time(),
                                          fileType)
        renderer = appy.pod.renderer.Renderer(
           '%s/%s.odt' % (os.path.dirname(__file__), self.portal_type),
           {'avis': self}, tempFileName,
           pythonWithUnoPath=os.environ.get('PYTHON_WITH_UNO_PATH'))
        renderer.run()
        # Tell the browser that the resulting page contains PDF
        response.setHeader('Content-type', 'application/%s' % fileType)
        response.setHeader('Content-disposition',
                           'inline;filename="%s.%s"' % (self.id[:200], fileType))
        # Returns the doc and removes the temp file
        f = open(tempFileName, 'rb')
        doc = f.read()
        f.close()
        try:
            os.remove(tempFileName)
        except OSError:
            pass
        return doc

    def getPrefixedApType(self, de=False):
        '''Returns the friendly name for the kind of law (apType), prefixed
           with the right article in french (l'/le, or d'/de, depending on
           p_de).'''

        if de:
            prefixes = {'arreteGouv': "d'", 'arreteMin': "d'", 'decret': "de " }
        else:
            prefixes = {'arreteGouv': "l'", 'arreteMin': "l'", 'decret': "le " }
        prefix = prefixes[self.getApType()]
        friendlyApType = self.schema._fields['apType'].vocabulary.getValue(
           self.getApType())
        return '%s%s' % (prefix, friendlyApType)

    def getCurrentDate(self):
        now = DateTime()
        return now.strftime('%d/%m/%Y')

    def getYesNoAnswer(self, q):
        if not q[0].islower():
            q = q[0].lower() + q[1:]
        t = self.getField(q).get(self)
        if t == 'yes':
            return 'oui'
        else:
            return 'non'
