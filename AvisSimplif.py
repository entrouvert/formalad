# -*- coding: utf-8 -*-
#
# GNU General Public License (GPL)
#
# This program is free software; you can redistribute it and/or
# modify it under the terms of the GNU General Public License
# as published by the Free Software Foundation; either version 2
# of the License, or (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program; if not, write to the Free Software
# Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA
# 02110-1301, USA.
#

__author__ = """Gaetan Delannay <gaetan.delannay at easi.wallonie.be>"""
__docformat__ = 'plaintext'

from AccessControl import ClassSecurityInfo
from DateTime import DateTime
from Products.Archetypes.atapi import *
from config import *
import os, time, os.path
import appy.pod.renderer
from Products.MasterSelectWidget.MasterSelectWidget import MasterSelectWidget

from Avis import AvisOdt

# ------------------------------------------------------------------------------
def slaveFields(action, *slaves):
    '''Helps to define more smartly the slave fields of a master field
       (MasterSelectWidget). The master is always a "yes/no" field.
       If p_action is:
       - "showIfYes": slaves will be shown when master is "yes" ;
       - "showIfNo": slaves will be shown when master is "no" ;
       - "setValue": it defines slaves whose values may change depending on
         actions on the master. '''
    res = []
    for slave in slaves:
        slaveDict = {'name': slave}
        if action == "showIfYes":
            slaveDict.update({'action': 'show', 'hide_values': ['yes'] })
        elif action == "showIfNo":
            slaveDict.update({'action': 'show', 'hide_values': ['no'] })
        elif action == "setValue":
            sName = slave[:-7]
            slaveDict.update({'action': 'value',
                              'vocab_method': 'defo%s' % sName,
                              'control_param': '%sCP' % sName})
        res.append(slaveDict)
    return tuple(res)

# ------------------------------------------------------------------------------
# Default values for "analysis" fields.

defaultAnalysisValues = {
    'delaiOrdre': \
        {'yes': u"Le texte en projet prévoit des délais d'ordre à l'article...",
         'no': ''
        },
    'delaiRigueur': \
        {'yes': u"Le texte en projet prévoit des délais de rigueur à l'article...",
         'no': ''
        },
    'delaiAccuse': \
        {'yes': u"Le texte en projet prévoit des délais d'accusés de réception à l'article...",
         'no': ''
        },
    'envoi': \
        {'yes': "",
         'no': ""
        },
    'piecesJustificativesDemandees': \
        {'yes': u"Le texte en projet requiert l'adjonction de pièces justificatives à l'article...",
         'no': ''
        },
    'simulRenvoi': \
        {'yes': u"Un renvoi de modalités est prévu à l'article...",
         'no': ''
        },
    'simulCirculaire': \
        {'yes': "",
         'no': ""
        },
    'simulBrochure': \
        {'yes': "",
         'no': ''
        },
    'acteurs': \
        {'yes': "",
         'no': ''
        },
    'formulairePrevu': \
        {'yes': u"Un formulaire est prévu.",
         'no': ''
        },
    'formulaireAttache': \
        {'yes': '',
         'no': ''
        },
    'charges': \
        {'yes': '',
         'no': ''
        },
    'vide': \
        {'yes': '',
         'no': ''
        },
}

defaultRecommandationsValues = {
    'piecesJustificativesDemandeesRecommandations': "",
}

schema = Schema((

    # Attributs liés au texte en projet ----------------------------------------

    StringField(
        name='apType',
        widget=SelectionWidget(
            label="Type de norme",
            format="select",
            label_msgid='Avis_label_apType',
            i18n_domain='Avis',
        ),
        enforceVocabulary=True,
        vocabulary=DisplayList(( ('arreteGouv', u'arrêté du Gouvernement wallon'),
                                 ('arreteMin', u'arrêté ministériel'),
                                 ('decret', u'décret') )),
        required=True
    ),

    StringField(
        name='apTitle',
        widget=TextAreaWidget(
            label=u"Intitulé de la norme",
            rows=3,
            label_msgid='Avis_label_apTitle',
            i18n_domain='Avis',
        ),
        required=True
    ),

    StringField(
        name='apDescription',
        widget=TextAreaWidget(
            label=u"Contexte",
            rows=5,
            label_msgid='Avis_label_apDescription',
            i18n_domain='Avis',
        )
    ),

    # Les délais ---------------------------------------------------------------

    StringField(
        name='delai',
        widget=MasterSelectWidget(
            label=u"Est-il pertinent d'aborder la question des délais ?",
            slave_fields= slaveFields('showIfYes',
                'delaiOrdre', 'delaiOrdreAnalyse', 'delaiRigueur',
                'delaiRigueurAnalyse', 'delaiAccuse', 'delaiAccuseAnalyse'),
            label_msgid='Avis_label_delai',
            i18n_domain='Avis',
        ),
        enforceVocabulary=True,
        vocabulary='getResponses',
        required=True
    ),

    StringField(
        name='delaiOrdre',
        widget=MasterSelectWidget(
            label=u"Y a-t-il des délais d'ordre définis, et ce de manière satisfaisante ?",
            slave_fields=slaveFields('setValue', 'delaiOrdreAnalyse'),
            label_msgid='Avis_label_delaiOrdre',
            i18n_domain='Avis',
        ),
        enforceVocabulary=True,
        vocabulary='getResponses',
        required=True
    ),

    TextField(
        name='delaiOrdreAnalyse',
        widget=TextAreaWidget(
            label=u"Analyse",
            label_msgid='Avis_label_delaiOrdreAnalyse',
            i18n_domain='Avis',
            rows=10,
        ),
        default_output_type="text/html",
        default=defaultAnalysisValues['delaiOrdre']['no']
    ),

    StringField(
        name='delaiRigueur',
        widget=MasterSelectWidget(
            label=u"Y a-t-il des délais de rigueur définis, et ce de manière satisfaisante ?",
            slave_fields=slaveFields('setValue', 'delaiRigueurAnalyse'),
            label_msgid='Avis_label_delaiRigueur',
            i18n_domain='Avis',
        ),
        enforceVocabulary=True,
        vocabulary='getResponses',
        required=True
    ),

    TextField(
        name='delaiRigueurAnalyse',
        widget=TextAreaWidget(
            label=u"Analyse",
            label_msgid='Avis_label_delaiRigueurAnalyse',
            i18n_domain='Avis',
            rows=10,
        ),
        default_output_type="text/html",
        default=defaultAnalysisValues['delaiRigueur']['no']
    ),

    StringField(
        name='delaiAccuse',
        widget=MasterSelectWidget(
            label=u"Y a-t-il un délai d'accusé de réception défini, et ce de manière satisfaisante ?",
            slave_fields=slaveFields('setValue', 'delaiAccuseAnalyse'),
            label_msgid='Avis_label_delaiAccuse',
            i18n_domain='Avis',
        ),
        enforceVocabulary=True,
        vocabulary='getResponses',
        required=True
    ),

    TextField(
        name='delaiAccuseAnalyse',
        widget=TextAreaWidget(
            label=u"Analyse",
            label_msgid='Avis_label_delaiAccuseAnalyse',
            i18n_domain='Avis',
            rows=10,
        ),
        default_output_type="text/html",
        default=defaultAnalysisValues['delaiAccuse']['no']
    ),

    # Les envois ---------------------------------------------------------------

    StringField(
        name='envoi',
        widget=MasterSelectWidget(
            label=u"Est-il pertinent d'aborder la question de la transmission de documents ?",
            slave_fields= slaveFields('showIfYes', 'envoiAnalyse', 'envoiRecommande'),
            label_msgid='Avis_label_envoi',
            i18n_domain='Avis',
        ),
        enforceVocabulary=True,
        vocabulary='getResponses',
        required=True
    ),

    StringField(
        name='envoiRecommande',
        widget=MasterSelectWidget(
            label=u"Y a-t-il des envois recommandés ?",
            label_msgid='Avis_label_envoiRecommande',
            i18n_domain='Avis',
        ),
        enforceVocabulary=True,
        vocabulary='getResponses',
        required=True
    ),

    TextField(
        name='envoiAnalyse',
        widget=TextAreaWidget(
            label=u"Analyse",
            label_msgid='Avis_label_envoiAnalyse',
            i18n_domain='Avis',
            rows=10,
        ),
        default_output_type="text/html",
        default=defaultAnalysisValues['envoi']['no']
    ),

    # Le principe de confiance -------------------------------------------------

    StringField(
        name='piecesJustificatives',
        widget=MasterSelectWidget(
            label=u"Est-il pertinent d'aborder la question du principe de confiance ?",
            slave_fields= slaveFields('showIfYes', 'piecesJustificativesDemandees',
                                      'piecesJustificativesDemandeesAnalyse'),
            label_msgid='Avis_label_piecesJustificatives',
            i18n_domain='Avis',
        ),
        enforceVocabulary=True,
        vocabulary='getResponses',
        required=True
    ),

    StringField(
        name='piecesJustificativesDemandees',
        widget=MasterSelectWidget(
            label=u"Des pièces justificatives sont-elles demandées ?",
            slave_fields=slaveFields('setValue', 'piecesJustificativesDemandeesAnalyse'),
            label_msgid='Avis_label_piecesJustificativesDemandees',
            i18n_domain='Avis',
        ),
        enforceVocabulary=True,
        vocabulary='getResponses',
        required=True
    ),

    TextField(
        name='piecesJustificativesDemandeesAnalyse',
        widget=TextAreaWidget(
            label=u"Analyse",
            label_msgid='Avis_label_piecesJustificativesDemandeesAnalyse',
            i18n_domain='Avis',
            rows=10,
        ),
        default_output_type="text/html",
        default=defaultAnalysisValues['piecesJustificativesDemandees']['no']
    ),

    TextField(
        name='piecesJustificativesDemandeesRecommandations',
        widget=TextAreaWidget(
            label=u"Recommandation(s)",
            label_msgid='Avis_label_piecesJustificativesDemandeesRecommandations',
            i18n_domain='Avis',
            rows=10,
        ),
        default_output_type="text/html",
        default=defaultRecommandationsValues['piecesJustificativesDemandeesRecommandations']
    ),

    # La simultanéité des textes -----------------------------------------------

    StringField(
        name='simultaneite',
        widget=MasterSelectWidget(
            label=u"Est-il pertinent d'aborder la question de la simultanéité des textes ?",
            slave_fields= slaveFields('showIfYes', 'simulRenvoi', 'simulRenvoiAnalyse',),
            label_msgid='Avis_label_simultaneite',
            i18n_domain='Avis',
        ),
        enforceVocabulary=True,
        vocabulary='getResponses',
        required=True
    ),

     StringField(
        name='simulRenvoi',
        widget=MasterSelectWidget(
            label=u"Y a-t-il renvoi de modalité vers un niveau de réglementation inférieur ?",
            slave_fields=slaveFields('setValue', 'simulRenvoiAnalyse'),
            label_msgid='Avis_label_simulRenvoi',
            i18n_domain='Avis',
        ),
        enforceVocabulary=True,
        vocabulary='getResponses',
        required=True
    ),

    TextField(
        name='simulRenvoiAnalyse',
        widget=TextAreaWidget(
            label=u"Analyse",
            label_msgid='Avis_label_simulRenvoiAnalyse',
            i18n_domain='Avis',
            rows=10,
        ),
        default_output_type="text/html",
        default=defaultAnalysisValues['simulRenvoi']['no']
    ),

    # La communication aux usagers et administrations --------------------------

    StringField(
        name='communication',
        widget=MasterSelectWidget(
            label=u"Est-il pertinent d'aborder la question de la communication aux usagers et administrations ?",
            slave_fields= slaveFields('showIfYes',
                'simulCirculaire',
                'simulCirculaireAnalyse', 'simulBrochure', 'simulBrochureAnalyse'),
            label_msgid='Avis_label_communication',
            i18n_domain='Avis',
        ),
        enforceVocabulary=True,
        vocabulary='getResponses',
        required=True
    ),

    StringField(
        name='simulCirculaire',
        widget=MasterSelectWidget(
            label=u"Une information est-elle nécessaire pour les Administrations (Circulaire) ?",
            slave_fields=slaveFields('setValue', 'simulCirculaireAnalyse'),
            label_msgid='Avis_label_simulCirculaire',
            i18n_domain='Avis',
        ),
        enforceVocabulary=True,
        vocabulary='getResponses',
        required=True
    ),

    TextField(
        name='simulCirculaireAnalyse',
        widget=TextAreaWidget(
            label=u"Analyse",
            label_msgid='Avis_label_simulCirculaireAnalyse',
            i18n_domain='Avis',
            rows=10,
        ),
        default_output_type="text/html",
        default=defaultAnalysisValues['simulCirculaire']['no']
    ),

    StringField(
        name='simulBrochure',
        widget=MasterSelectWidget(
            label=u"Une information est-elle nécessaire pour les usagers (brochure) ?",
            slave_fields=slaveFields('setValue', 'simulBrochureAnalyse'),
            label_msgid='Avis_label_simulBrochure',
            i18n_domain='Avis',
        ),
        enforceVocabulary=True,
        vocabulary='getResponses',
        required=True
    ),

    TextField(
        name='simulBrochureAnalyse',
        widget=TextAreaWidget(
            label=u"Analyse",
            label_msgid='Avis_label_simulBrochureAnalyse',
            i18n_domain='Avis',
            rows=10,
        ),
        default_output_type="text/html",
        default=defaultAnalysisValues['simulBrochure']['no']
    ),

    # La prise en compte des acteurs -------------------------------------------

    StringField(
        name='acteurs',
        widget=MasterSelectWidget(
            label=u"Est-il pertinent d'aborder la question de la prise en compte des acteurs ?",
            slave_fields= slaveFields('showIfYes', 'acteursAnalyse'),
            label_msgid='Avis_label_acteurs',
            i18n_domain='Avis',
        ),
        enforceVocabulary=True,
        vocabulary='getResponses',
        required=True
    ),

    TextField(
        name='acteursAnalyse',
        widget=TextAreaWidget(
            label=u"Analyse",
            label_msgid='Avis_label_acteursAnalyse',
            i18n_domain='Avis',
            rows=10,
        ),
        default_output_type="text/html",
        default=defaultAnalysisValues['acteurs']['no']
    ),

    # Formulaires --------------------------------------------------------------

    StringField(
        name='formulaires',
        widget=MasterSelectWidget(
            label=u"Est-il pertinent d'aborder la question des formulaires ?",
            slave_fields= slaveFields('showIfYes',
                'formulairePrevu', 'formulairePrevuAnalyse',
                'formulaireAttache', 'formulaireAttacheAnalyse'),
            label_msgid='Avis_label_formulaires',
            i18n_domain='Avis',
        ),
        enforceVocabulary=True,
        vocabulary='getResponses',
        required=True
    ),

    StringField(
        name='formulairePrevu',
        widget=MasterSelectWidget(
            label=u"Un formulaire est-il prévu ?",
            slave_fields=slaveFields('setValue', 'formulairePrevuAnalyse'),
            label_msgid='Avis_label_formulairePrevu',
            i18n_domain='Avis',
        ),
        enforceVocabulary=True,
        vocabulary='getResponses',
        required=True
    ),

    TextField(
        name='formulairePrevuAnalyse',
        widget=TextAreaWidget(
            label=u"Analyse",
            label_msgid='Avis_label_formulairePrevuAnalyse',
            i18n_domain='Avis',
            rows=10,
        ),
        default_output_type="text/html",
        default=defaultAnalysisValues['formulairePrevu']['no']
    ),

    StringField(
        name='formulaireAttache',
        widget=MasterSelectWidget(
            label=u"Le formulaire est-il attaché au texte ?",
            slave_fields=slaveFields('setValue', 'formulaireAttacheAnalyse'),
            label_msgid='Avis_label_formulaireAttache',
            i18n_domain='Avis',
        ),
        enforceVocabulary=True,
        vocabulary='getResponses',
        required=True
    ),

    TextField(
        name='formulaireAttacheAnalyse',
        widget=TextAreaWidget(
            label=u"Analyse",
            label_msgid='Avis_label_formulaireAttacheAnalyse',
            i18n_domain='Avis',
            rows=10,
        ),
        default_output_type="text/html",
        default=defaultAnalysisValues['formulaireAttache']['no']
    ),

    #  charges administratives ------------------------------------

    StringField(
        name='charges',
        widget=MasterSelectWidget(
            label=u"Est-il pertinent d'aborder la question des charges administratives ?",
            slave_fields= slaveFields('showIfYes', 'chargesAnalyse'),
            label_msgid='Avis_label_charges',
            i18n_domain='Avis',
        ),
        enforceVocabulary=True,
        vocabulary='getResponses',
        required=True
    ),

    TextField(
        name='chargesAnalyse',
        widget=TextAreaWidget(
            label=u"Analyse",
            label_msgid='Avis_label_chargesAnalyse',
            i18n_domain='Avis',
            rows=10,
        ),
        default_output_type="text/html",
        default=defaultAnalysisValues['charges']['yes']
    ),

    #  amélioration de la réglementation --------------------------

    StringField(
        name='ameliorationReglementation',
        widget=MasterSelectWidget(
            label=u"Est-il pertinent d'aborder la question de l'amélioration de la réglementation ?",
            slave_fields= slaveFields('showIfYes', 'ameliorationReglementationExemples'),
            label_msgid='Avis_label_ameliorationReglementation',
            i18n_domain='Avis',
        ),
        enforceVocabulary=True,
        vocabulary='getResponses',
        required=True
    ),

    TextField(
        name='ameliorationReglementationExemples',
        widget=TextAreaWidget(
            label=u"Exemples",
            label_msgid='Avis_label_ameliorationReglementationExemples',
            i18n_domain='Avis',
            rows=10,
        ),
        default_output_type="text/html",
        default=defaultAnalysisValues['vide']['yes']
    ),

    #  mesures liées à l'egouvernement ----------------------------

    StringField(
        name='egouv',
        widget=MasterSelectWidget(
            label=u"Est-il pertinent d'aborder la question des mesures liées à l'e-gouvernement ?",
            slave_fields= slaveFields('showIfYes', 'egouvExemples'),
            label_msgid='Avis_label_charges',
            i18n_domain='Avis',
        ),
        enforceVocabulary=True,
        vocabulary='getResponses',
        required=True
    ),

    TextField(
        name='egouvExemples',
        widget=TextAreaWidget(
            label=u"Exemples",
            label_msgid='Avis_label_egouvExemples',
            i18n_domain='Avis',
            rows=10,
        ),
        default_output_type="text/html",
        default=defaultAnalysisValues['vide']['yes']
    ),

    #  accès aux sources authentiques et partages de données ------

    StringField(
        name='sources',
        widget=MasterSelectWidget(
            label=u"Est-il pertinent d'aborder la question de l'accès aux sources authentiques et partages de données ?",
            slave_fields= slaveFields('showIfYes', 'sourcesAnalyse'),
            label_msgid='Avis_label_sources',
            i18n_domain='Avis',
        ),
        enforceVocabulary=True,
        vocabulary='getResponses',
        required=True
    ),

    TextField(
        name='sourcesAnalyse',
        widget=TextAreaWidget(
            label=u"Analyse",
            label_msgid='Avis_label_Analyse',
            i18n_domain='Avis',
            rows=10,
        ),
        default_output_type="text/html",
        default=defaultAnalysisValues['vide']['yes']
    ),

),
)

MyBaseSchema = BaseSchema.copy()
AvisSimplif_schema = MyBaseSchema.copy() + schema.copy()

class AvisSimplif(BaseContent, AvisOdt):
    '''Critique orientée simplification d'un texte'''
    security = ClassSecurityInfo()
    __implements__ = (getattr(BaseContent,'__implements__',()),)

    # This name appears in the 'add' box
    archetype_name = 'Avis de simplification'
    meta_type = 'AvisSimplif'
    portal_type = 'AvisSimplif'
    allowed_content_types = []
    filter_content_types = 0
    global_allow = 1
    #content_icon = 'Avis.gif'
    immediate_view = 'base_view'
    default_view = 'base_view'
    suppl_views = ()
    typeDescription = "AvisSimplif"
    typeDescMsgId = 'description_edit_avis_simplif'

    actions =  (
       {'action': "string:$object_url/generateOdt",
        'category': "document_actions",
        'id': 'asOdt',
        'name': u'Générer en ODT',
        'permissions': ("View",),
        'condition': 'python:1'
       },
       {'action': "string:$object_url/generateDoc",
        'category': "document_actions",
        'id': 'asDoc',
        'name': u'Générer au format Microsoft Word',
        'permissions': ("View",),
        'condition': 'python:1'
       },
    )

    _at_rename_after_creation = True
    schema = AvisSimplif_schema

    security.declarePublic('generateOdt')
    def generateOdt(self, RESPONSE):
        '''Generates the ODT version of this advice.'''
        return self._generate(RESPONSE, 'odt')

    security.declarePublic('generateDoc')
    def generateDoc(self, RESPONSE):
        '''Generates the Doc version of this advice.'''
        return self._generate(RESPONSE, 'doc')

    security.declarePublic('getResponses')
    def getResponses(self):
        """Returns the predefined responses that are possible for each question.
        """
        return DisplayList( (('no', 'Non'), ('yes', 'Oui')) )

    security.declarePublic('getResponsesInversed')
    def getResponsesInversed(self):
        """Returns the predefined responses that are possible for each question.
        """
        return DisplayList( (('yes', 'Oui'), ('no', 'Non')) )

    def isEmpty(self, fieldName):
        '''Is the rich text field p_fieldName empty?'''
        accessor = 'get%s%s' % (fieldName[0].upper(), fieldName[1:])
        fieldContent = getattr(self, accessor)()
        return fieldContent.strip() == ''

    def getCurrentDate(self):
        now = DateTime()
        return now.strftime('%d/%m/%Y')

    def getUserName(self):
        userInfo = self.portal_membership.getMemberById(self.Creator())
        return userInfo.getProperty('fullname').decode('latin-1')

    def getUserEmail(self):
        userInfo = self.portal_membership.getMemberById(self.Creator())
        return userInfo.getProperty('email').decode('latin-1')

    # Methods for getting default answers to questions -------------------------
    def defodelaiOrdre(self, delaiOrdreCP):
        if self.delaiOrdreAnalyse(): return unicode(self.delaiOrdreAnalyse(), 'utf-8')
        return defaultAnalysisValues['delaiOrdre'][delaiOrdreCP]

    def defodelaiRigueur(self, delaiRigueurCP):
        if self.delaiRigueurAnalyse(): return unicode(self.delaiRigueurAnalyse(), 'utf-8')
        return defaultAnalysisValues['delaiRigueur'][delaiRigueurCP]

    def defodelaiAccuse(self, delaiAccuseCP):
        if self.delaiAccuseAnalyse(): return unicode(self.delaiAccuseAnalyse(), 'utf-8')
        return defaultAnalysisValues['delaiAccuse'][delaiAccuseCP]

    def defoenvoiTermes(self, envoiTermesCP):
        if self.envoiTermesAnalyse(): return unicode(self.envoiTermesAnalyse(), 'utf-8')
        return defaultAnalysisValues['envoiTermes'][envoiTermesCP]

    def defoenvoiModalites(self, envoiModalitesCP):
        if self.envoiModalitesAnalyse(): return unicode(self.envoiModalitesAnalyse(), 'utf-8')
        return defaultAnalysisValues['envoiModalites'][envoiModalitesCP]

    def defoenvoiRecommande(self, envoiRecommandeCP):
        if self.envoiRecommandeAnalyse(): return unicode(self.envoiRecommandeAnalyse(), 'utf-8')
        return defaultAnalysisValues['envoiRecommande'][envoiRecommandeCP]

    def defopiecesJustificativesDemandees(self, piecesJustificativesDemandeesCP):
        if self.piecesJustificativesDemandeesAnalyse(): return unicode(self.piecesJustificativesDemandeesAnalyse(), 'utf-8')
        return defaultAnalysisValues['piecesJustificativesDemandees'][piecesJustificativesDemandeesCP]

    def defosimulRenvoi(self, simulRenvoiCP):
        if self.simulRenvoiAnalyse(): return unicode(self.simulRenvoiAnalyse(), 'utf-8')
        return defaultAnalysisValues['simulRenvoi'][simulRenvoiCP]

    def defosimulCirculaire(self, simulCirculaireCP):
        if self.simulCirculaireAnalyse(): return unicode(self.simulCirculaireAnalyse(), 'utf-8')
        return defaultAnalysisValues['simulCirculaire'][simulCirculaireCP]

    def defosimulBrochure(self, simulBrochureCP):
        if self.simulBrochureAnalyse(): return unicode(self.simulBrochureAnalyse(), 'utf-8')
        return defaultAnalysisValues['simulBrochure'][simulBrochureCP]

    def defoformulairePrevu(self, formulairePrevuCP):
        if self.formulairePrevuAnalyse(): return unicode(self.formulairePrevuAnalyse(), 'utf-8')
        return defaultAnalysisValues['formulairePrevu'][formulairePrevuCP]

    def defoformulaireAttache(self, formulaireAttacheCP):
        if self.formulaireAttacheAnalyse(): return unicode(self.formulaireAttacheAnalyse(), 'utf-8')
        return defaultAnalysisValues['formulaireAttache'][formulaireAttacheCP]

registerType(AvisSimplif, PROJECTNAME)
# end of class AvisSimplif

##code-section module-footer #fill in your manual code here
##/code-section module-footer



