## Script (Python) "addLegis.cpy"
##bind container=container
##bind context=context
##bind namespace=
##bind script=script
##bind subpath=traverse_subpath
##title=Add a legistic part
##parameters=
from Products.CMFCore.utils import getToolByName

request = context.REQUEST

context.invokeFactory('AvisLegis', 'legis')

return request.RESPONSE.redirect(request.URL1 + '/legis/edit')

