## Script (Python) "addSimplif.cpy"
##bind container=container
##bind context=context
##bind namespace=
##bind script=script
##bind subpath=traverse_subpath
##title=Add a simplification
##parameters=
from Products.CMFCore.utils import getToolByName

request = context.REQUEST

context.invokeFactory('AvisSimplif', 'simplif')

return request.RESPONSE.redirect(request.URL1 + '/simplif/edit')

