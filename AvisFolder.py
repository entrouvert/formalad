# -*- coding: utf-8 -*-
##
# GNU General Public License (GPL)
#
# This program is free software; you can redistribute it and/or
# modify it under the terms of the GNU General Public License
# as published by the Free Software Foundation; either version 2
# of the License, or (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program; if not, write to the Free Software
# Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA
# 02110-1301, USA.
#

__author__ = """Gaetan Delannay <gaetan.delannay at easi.wallonie.be>"""
__docformat__ = 'plaintext'

from AccessControl import ClassSecurityInfo
from Products.Archetypes.atapi import *
from config import *

schema = Schema((
),
)

AvisFolder_schema = OrderedBaseFolderSchema.copy() + \
    schema.copy()

class AvisFolder(OrderedBaseFolder):
    '''Dossier contenant des critiques de texte'''
    security = ClassSecurityInfo()
    __implements__ = (getattr(OrderedBaseFolder,'__implements__',()),)

    # This name appears in the 'add' box
    archetype_name = "Dossier d'avis"
    meta_type = 'AvisFolder'
    portal_type = 'AvisFolder'
    allowed_content_types = ['AvisSimplif', 'AvisLegis', 'AvisFolder']
    filter_content_types = 1
    global_allow = 1
    #content_icon = 'AvisFolder.gif'
    immediate_view = 'base_view'
    default_view = 'base_view'
    suppl_views = ()
    typeDescription = "AvisFolder"
    typeDescMsgId = 'description_edit_avisfolder'

    _at_rename_after_creation = True
    schema = AvisFolder_schema

registerType(AvisFolder, PROJECTNAME)
