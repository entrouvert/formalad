# -*- coding: utf-8 -*-
#
# GNU General Public License (GPL)
#
# This program is free software; you can redistribute it and/or
# modify it under the terms of the GNU General Public License
# as published by the Free Software Foundation; either version 2
# of the License, or (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program; if not, write to the Free Software
# Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA
# 02110-1301, USA.
#

__author__ = """Frederic Peters <fpeters@entrouvert.com>"""
__docformat__ = 'plaintext'

from AccessControl import ClassSecurityInfo
from DateTime import DateTime
from Products.Archetypes.atapi import *
from config import *
import os, time, os.path
import appy.pod.renderer
from Products.MasterSelectWidget.MasterSelectWidget import MasterSelectWidget

from Avis import AvisOdt

# ------------------------------------------------------------------------------
def slaveFields(action, *slaves):
    '''Helps to define more smartly the slave fields of a master field
       (MasterSelectWidget). The master is always a "yes/no" field.
       If p_action is:
       - "showIfYes": slaves will be shown when master is "yes" ;
       - "showIfNo": slaves will be shown when master is "no" ;
       - "setValue": it defines slaves whose values may change depending on
         actions on the master. '''
    res = []
    for slave in slaves:
        slaveDict = {'name': slave}
        if action == "showIfYes":
            slaveDict.update({'action': 'show', 'hide_values': ['yes'] })
        elif action == "showIfNo":
            slaveDict.update({'action': 'show', 'hide_values': ['no'] })
        elif action == "setValue":
            sName = slave[:-7]
            slaveDict.update({'action': 'value',
                              'vocab_method': 'defo%s' % sName,
                              'control_param': '%sCP' % sName})
        res.append(slaveDict)
    return tuple(res)

# ------------------------------------------------------------------------------
# Default values for "analysis" fields.

defaultAnalysisValues = {

    'directive_analyse': \
      {'yes': u'''''',
       'no' : ''
      },

    'directive_analyse_article1_analyse': \
      {'yes': '',
       'no' : u'''Insérez la phrase suivante :
«  Article 1er .Le présent décret/arrêté transpose
(partiellement) la directive… »'''
      },

    'directive_analyse_alteration_analyse': \
      {'yes': '',
       'no' : u'''Vous devez intégrer les définitions sans altérer leur contenu. La
définition suivante doit être revue : « … »'''
      },

    'directive_analyse_reference_analyse': \
      {'yes': u'''Les dispositions suivantes de la directive sont transposées par
référence. Elles ne sont donc pas transposées de manière effective.''',
       'no' : ''
      },

    'directive_analyse_litteralement_analyse': \
      {'yes': u'''Ne recopiez pas servilement : les règles de droit interne doivent
atteindre le résultat voulu par la Directive.''',
       'no' : ''
      },

    'directive_analyse_particuliers_analyse': \
      {'yes': '',
       'no' : u'''Les dispositions suivantes ne doivent pas être transposées , elles
concernent exclusivement les autorités des Etats membres ou les organes de
la Communauté :..'''
      },

    'directive_analyse_regionales_analyse': \
      {'yes': '',
       'no' : u'''Les dispositions suivantes doivent être retirées : elles excèdent
les compétences de la Région wallonne.'''
      },

    'directive_analyse_circulaire_analyse': \
      {'yes': u'''Une directive ne peut pas être transposée par une circulaire : il
faut la transposer  di par la voie décrétale ou réglementaire.''',
       'no' : ''
      },

    'traite_analyse': \
      {'yes': u'''''',
       'no' : ''
      },

    'traite_analyse_constitution_analyse': \
      {'yes': '',
       'no' : u'''L’avant-projet est contraire à l’article…de la Constitution.'''
      },

    'traite_analyse_plusieurs_analyse': \
      {'yes': u'''''',
       'no' : ''
      },

    'traite_analyse_plusieurs_analyse_distincts_analyse': \
      {'yes': '',
       'no' : u'''Rédigez un article distinct pour chaque traité.'''
      },

    'cooperation_analyse': \
      {'yes': u'''''',
       'no' : ''
      },

    'cooperation_analyse_parties_analyse': \
      {'yes': '',
       'no' : u'''La procédure d’assentiment ne peut pas débuter car l’accord n’a
pas encore été signé par toutes les parties'''
      },

    'precis_analyse': \
      {'yes': '',
       'no' : u'''L’intitulé pourrait/devrait être rédigé de la manière
suivante : « … ».'''
      },

    'decret_analyse': \
      {'yes': u'''''',
       'no' : ''
      },

    'decret_analyse_presentation_analyse': \
      {'yes': u'''''',
       'no' : ''
      },

    'decret_analyse_presentation_analyse_bien_redige_analyse': \
      {'yes': '',
       'no' : u'''Un arrêté de présentation de l’avant-projet de décret
devrait être rédigé comme suit :
« Le Gouvernement wallon,
Sur la proposition du (de la) Ministre de… [ titre officiel
mais limité à la compétence en vertu de laquelle il agit ] ,
Après délibération,
ARRETE :
Le (La) Ministre de …est chargé(e) de présenter au Parlement
wallon le projet de décret dont la teneur suit :         ».'''
      },

    'decret_analyse_presentation_analyse': \
      {'yes': '',
       'no' : u'''Il faut prévoir un arrêté de présentation, rédigé de la manière
suivante :
« Le Gouvernement wallon,
Sur la proposition du (de la) Ministre de… [ titre officiel mais limité à
la compétence en vertu de laquelle il agit ] ,
Après délibération,
ARRETE :
Le (La) Ministre de …est chargé(e) de présenter au Parlement wallon le
projet de décret dont la teneur suit :         ».'''
      },

    'debut_analyse': \
      {'yes': '',
       'no' : u'''Le préambule ne mentionne pas la formule de début :
Insérez les mots suivants : « Le Gouvernement wallon,.. »'''
      },

    'reglement_analyse': \
      {'yes': '',
       'no' : u'''Le préambule ne mentionne pas tout règlement européen qui est exécuté par
l’arrêté :
Insérez la phrase suivante : « Vu la directive/le règlement…….. »'''
      },

    'actes_fondement_analyse': \
      {'yes': '',
       'no' : u'''Le préambule ne mentionne pas les actes de droit interne et les
articles qui constituent le fondement juridique de l’arrêté :
Insérez la phrase suivante : « Vu……………….. »'''
      },

    'actes_modifies_analyse': \
      {'yes': '',
       'no' : u'''Le préambule ne mentionne pas les actes de droit interne qui sont
modifié, abrogés ou retirés par l’arrêté :
Insérez la phrase suivante : « Vu…………………… »'''
      },

    'formalites_analyse': \
      {'yes': '',
       'no' : u'''Le préambule ne mentionne pas les formalités obligatoires accomplies
lors de l’élaboration de l’arrêté ou les raisons de leur
inaccomplissement :
Insérez la (les)phrase(s) suivante(s) :
« Vu l’avis…(numéro éventuel) du …(organe consulté), donné
le..(date) » ;
« Vu l’avis de l’Inspecteur des Finances, donné le…(date) » ;
« Vu l’accord du (de la) Ministre du Budget, donné le…(date) » ;
« Vu l’avis (numéro) du Conseil d’Etat, donné le…(date), en
application de l’article…………..des lois sur le Conseil d’Etat, coordonnées
le 12 janvier 1973.'''
      },

    'ministres_analyse': \
      {'yes': '',
       'no' : u'''Le préambule ne mentionne pas les Ministres qui proposent l’arrêté ainsi
que la délibération du Gouvernement :
Insérez la phrase suivante :
« Sur la proposition du (de la) Ministre de…..,
Après délibération, »'''
      },

    'formalites_non_obligatoires_analyse': \
      {'yes': '',
       'no' : u'''Vous pouvez mentionner les formalités non obligatoires que vous avez
accomplies.'''
      },

    'justification_arrete_analyse': \
      {'yes': '',
       'no' : u'''Vous pouvez mentionner la justification de la prise de l’arrêté, même si
sa motivation formelle n’est pas imposée par une norme.'''
      },

    'mot_final_analyse': \
      {'yes': '',
       'no' : u'''Le préambule ne mentionne pas le mot final.
Insérez le mot suivant « Arrête.. »'''
      },

    'norme_particuliere_analyse': \
      {'yes': u'''Il existe une norme particulière qui impose une motivation formelle pour
cet arrêté : vous pouvez mentionner les considérants relatifs à cette
motivation.''',
       'no' : ''
      },

    'unique_analyse': \
      {'yes': u'''''',
       'no' : ''
      },

    'unique_analyse_correctement_indique_analyse': \
      {'yes': '',
       'no' : u'''L’article unique n’est pas correctement indiqué :
Ecrivez en caractère gras : « Article unique .»'''
      },

    'unique_analyse': \
      {'yes': '',
       'no' : u''''''
      },

    'unique_analyse_numerotation_analyse': \
      {'yes': '',
       'no' : u'''Vous devez indiquer et numéroter les articles de la manière suivante  et
de façon continue du début à la fin du dispositif :
« Article 1er .», « Art. 2. », « Art. 3. »,…'''
      },

    'unique_analyse_intitule_analyse': \
      {'yes': u'''''',
       'no' : ''
      },

    'unique_analyse_intitule_analyse_specifique_analyse': \
      {'yes': '',
       'no' : u'''Vous devez rédiger un intitulé spécifique pour chaque
article.'''
      },

    'unique_analyse_intitule_analyse_fidele_analyse': \
      {'yes': '',
       'no' : u'''Vous devez choisir un intitulé qui reflète fidèlement et
complètement le contenu de l’article.'''
      },

    'unique_analyse_division_analyse': \
      {'yes': '',
       'no' : u'''La division des articles en alinéas et paragraphes n’est pas adéquate :
- l’article xxx contient un nombre trop élevé d’alinéas,
- tous les éléments de l’article xxx ne se retrouvent pas dans un
paragraphe,
- divisez les articles de la manière suivante : d’abord « § 1er. » et
ensuite « § 2. », « § 3. », « § 4. », etc.'''
      },

    'unique_analyse_numerotation_phrase_analyse': \
      {'yes': '',
       'no' : u'''Les énumérations doivent être présentées de la manière suivante :
« 1° », « 2° », « 3° »,…puis éventuellement « a) », « b) », « c) »,… '''
      },

    'unique_analyse_phrases_incidentes_analyse': \
      {'yes': u'''L’énumération contient des phrases incidentes :elles sont à proscrire
Enlevez la phrase suivante : « ………. » et placez la après l’énumération.''',
       'no' : ''
      },

    'unique_analyse_groupes_articles_analyse': \
      {'yes': '',
       'no' : u'''Vous devez grouper les articles de la façon suivante : Partie,  Livre,
Titre, Chapitre, Section, Sous-section .'''
      },

    'unique_analyse_une_division_analyse': \
      {'yes': '',
       'no' : u'''Tous les articles doivent se trouver dans une division :
Il y a lieu d’intégrer l’(es) article(s) suivant(s) dans une division.'''
      },

    'unique_analyse_chiffres_arabes_analyse': \
      {'yes': '',
       'no' : u'''Les divisions doivent être numérotées en chiffres arabes.'''
      },

    'unique_analyse_intitule_division_analyse': \
      {'yes': '',
       'no' : u'''L ‘intitulé de la division…… n’et pas précis/complet/concis. Il devrait
être rédigé comme suit : « …. »'''
      },

    'unique_analyse_references_analyse': \
      {'yes': '',
       'no' : u'''Les références externes/internes ne sont pas correcte.
Les références externes doivent être indiquées de la manière suivante :
A l’article…   , écrire :« article  , alinéa   , 3°, deuxième tiret ».
Dans les références internes, il ne faut pas indiquer « du présent
décret/arrêté », ni viser l’alinéa « précédent ».'''
      },

    'unique_analyse_reiteratives_analyse': \
      {'yes': u'''Le texte contient des dispositions réitératives :
L’alinéa…/Le paragraphe…/La phrase « … » doit être retiré (e) : il s’agit
du rappel d’une norme hiérarchiquement supérieure.''',
       'no' : ''
      },

    'unique_analyse_sans_portee_analyse': \
      {'yes': u'''Les articles……n’ont pas de portée normative : ils doivent être retirés.''',
       'no' : ''
      },

    'unique_analyse_portee_individuelle_analyse': \
      {'yes': u'''Les articles  …… doivent être retirés.  Les dispositions à portée
individuelle doivent faire l’objet d’un second arrêté à portée
individuelle.  Attention, cet arrêté constituera un acte administratif qui
devra être formellement motivé conformément à la loi du 29 juillet 1991
relative à la motivation formelle des actes administratifs.''',
       'no' : ''
      },

    'unique_analyse_parenthses_analyse': \
      {'yes': u'''Votre projet comporte des parenthèses/ des notes en bas de
page aux articles    . Elles doivent être retirées.''',
       'no' : ''
      },

    'unique_analyse_nombres_analyse': \
      {'yes': u'''''',
       'no' : ''
      },

    'unique_analyse_nombres_analyse_tres_eleve_analyse': \
      {'yes': u'''Il s’agit d’un nombre très élevé/d’une somme d’argent/d’une date/de la
référence à un article :
A l’article…..,  écrivez les nombres en chiffres.''',
       'no' : u'''Il ne s’agit pas d’un nombre très elevé/d’une somme d’argent/d’une date/de
la référence à un article :
A l’article…..,  écrivez les nombres en lettres.'''
      },

    'unique_analyse_sigle_analyse': \
      {'yes': u'''''',
       'no' : ''
      },

    'unique_analyse_sigle_analyse_lettres_analyse': \
      {'yes': '',
       'no' : u'''Le sigle (…)se prononce lettre
par lettre, il doit donc être écrit en séparant chaque lettre par un point.'''
      },

    'unique_analyse_sigle_analyse_points_analyse': \
      {'yes': '',
       'no' : u'''Le sigle (…)se prononce comme un mot ordinaire : il ne faut pas
mettre de point entre les lettres .'''
      },

    'unique_analyse_euros_analyse': \
      {'yes': u'''''',
       'no' : ''
      },

    'unique_analyse_euros_analyse_minuscule_analyse': \
      {'yes': '',
       'no' : u'''A l’article ( ), écrivez « euro » et « cent » avec une minuscule.'''
      },

    'unique_analyse_euros_analyse_eur_analyse': \
      {'yes': u'''Le code ISO « EUR » ne peut être employé que dans des tableaux, il doit
être enlevé à l’article ( )''',
       'no' : ''
      },

    'unique_analyse_euros_analyse_sigle_analyse': \
      {'yes': u'''A l’article ( ), le sigle « € » doit être retiré.''',
       'no' : ''
      },

    'unique_analyse_majuscule_analyse': \
      {'yes': '',
       'no' : u'''L’emploi de la majuscule n’est pas correct
A l’article ( ), la majuscule doit être retirée/ajoutée au mot « … »'''
      },

    'transfert_analyse': \
      {'yes': u'''''',
       'no' : ''
      },

    'transfert_analyse_article1_analyse': \
      {'yes': '',
       'no' : u'''L’article 1er doit être rédigé comme suit :
«  Article 1er. Le présent décret (arrêté) règle, en application de
l’article 138 de la Constitution, une matière visée à l’article 127 (à
l’article 128) de celle-ci. » 
N.B. L’article 127 concerne les matières culturelles,
l’article 128 concerne les matières personnalisables.'''
      },

    'matiere_regionale_analyse': \
      {'yes': u'''Rédigez deux projets : un projet de décret ou d’arrêté pour la matière
régionale et un autre projet de décret ou d’arrêté pour la matière
transférée. L’obligation de rédiger des projets distincts résulte du fait
que les actes concernés ont, selon la matière réglée, un champ
d’application territorial différent et des règles d’adoption différentes.''',
       'no' : ''
      },

    'definition_dictionnaire_analyse': \
      {'yes': u'''La définition du mot « .. » doit être retirée.''',
       'no' : ''
      },

    'definition_acte_analyse': \
      {'yes': '',
       'no' : u'''La définition du mot « … » doit être retirée.'''
      },

    'definition_acte_superieur_analyse': \
      {'yes': u'''La définition du mot « … » doit être retirée.''',
       'no' : ''
      },

    'definition_elements_analyse': \
      {'yes': u'''Les termes normatifs « …. » contenus dans la définition du mot « …. »
doivent être retirés.''',
       'no' : ''
      },

    'definition_portee_analyse': \
      {'yes': u'''La précision contenue à l’article (…) doit être retirée et ajoutée à la
définition du mot « … »''',
       'no' : ''
      },

    'denomination_acte_analyse': \
      {'yes': '',
       'no' : u'''La dénomination de la loi /le décret/ l’arrêté doit être complétée comme
suit : « … »'''
      },

    'definitions_explicites_analyse': \
      {'yes': u'''''',
       'no' : ''
      },

    'definitions_explicites_analyse_rassemblees_analyse': \
      {'yes': '',
       'no' : u'''Les définitions explicitent tout le texte auquel elles se
rapportent, rassemblez-les dans un article spécifique placé au début de
celui-ci, après les articles déterminant respectivement la matière réglée
et le champ d’application.'''
      },

    'definitions_explicites_analyse': \
      {'yes': '',
       'no' : u''''''
      },

    'definitions_explicites_analyse_un_mot_analyse': \
      {'yes': u'''''',
       'no' : ''
      },

    'definitions_explicites_analyse_un_mot_analyse_un_article_analyse': \
      {'yes': '',
       'no' : u'''La définition « … » concerne un mot utilisé dans un
seul article : énoncez-la uniquement dans cet article.'''
      },

    'defs_nombreuses_modifs_analyse': \
      {'yes': u'''Le projet initial a déjà fait l’objet de nombreuses modifications .Evaluez
l’opportunité ,pour l’ensemble du projet, de rédiger, soit des dispositions
modificatives, soit des dispositions autonomes accompagnées d’une
disposition abrogatoire supprimant entièrement l’acte originel.''',
       'no' : ''
      },

    'modifs_excessif_analyse': \
      {'yes': u'''Les articles……sont fortement modifiés.  Evaluez l’opportunité soit
de les remplacer, soit de les modifier.''',
       'no' : ''
      },

    'modifs_estethiques_analyse': \
      {'yes': u'''Les modifications prévues aux articles…..sont purement esthétiques. Elles
sont donc à déconseiller (si vous en faites, expliquez dans l’exposé des
motifs).''',
       'no' : ''
      },

    'modifs_arrete_analyse': \
      {'yes': u'''Le projet modifie/abroge un décret/une loi : c’est contraire à la
hiérarchie des normes.''',
       'no' : ''
      },

    'modifs_decret_analyse': \
      {'yes': u'''Le projet modifie un arrêté. Il faut respectez les compétences des autres
autorités : ne modifiez jamais un arrêté au moyen d’un acte législatif.''',
       'no' : ''
      },

    'modifs_successives_analyse': \
      {'yes': '',
       'no' : u'''Les modifications successives du texte…..ne sont pas correctement rédigées.
Désignez l’acte dont les dispositions sont modifiées en mentionnant sa
dénomination complète : arrêté du Gouvernement wallon du 2 janvier 2008
relatif à…Ensuite, indiquez simplement « du même décret », « du même
arrêté ».'''
      },

    'modifs_historique_analyse': \
      {'yes': '',
       'no' : u'''Mentionnez l’historique des modifications de l’article…..'''
      },

    'modifs_alinea_analyse': \
      {'yes': u'''Attention aux règles à suivre.''',
       'no' : ''
      },

    'modifs_acte_analyse': \
      {'yes': '',
       'no' : u'''Lorsque vous modifiez et abrogez plusieurs articles d’un même acte,
présentez les opérations relatives à ces articles en suivant leur ordre
numérique.'''
      },

    'numerotation_divisions_analyse': \
      {'yes': '',
       'no' : u'''La numérotation des divisions, des articles et des paragraphes insérés
n’est pas cohérente par rapport à la numérotation de base.
Numérotez les articles insérés « Art. 2/1. », « Art. 2/2. », « Art.
2/3. »,…ou « Art. 2bis »    Numérotez les divisions insérées : « Chapitre
3/1. », « Chapitre 3/2. »,…ou « Chapitre 2bis »,…'''
      },

    'renumerotation_analyse': \
      {'yes': u'''Les articles….ou la division….sont renumérotés. C’est déconseillé.''',
       'no' : ''
      },

    'modifs_plusieurs_textes_analyse': \
      {'yes': u'''Le projet modifie plusieurs actes : suivez leur ordre chronologique en
commençant par le plus ancien.''',
       'no' : ''
      },

    'nombreuses_modifs_analyse': \
      {'yes': u'''Les modifications sont nombreuses : évaluez l’opportunité de créer un
chapitre ou une section pour chaque acte à modifier et rassemblez-y toutes
les modifications qui le concernent.''',
       'no' : ''
      },

    'modifs_actes_legislatifs_analyse': \
      {'yes': u'''''',
       'no' : ''
      },

    'modifs_actes_legislatifs_analyse_d_abord_analyse': \
      {'yes': '',
       'no' : u'''Dans un même texte, vous modifiez des actes législatifs et
vous abrogez des arrêtés ;traitez d’abord tous les actes législatifs.'''
      },

    'modif_disposition_modif_analyse_en_vigueur_analyse': \
      {'yes': u'''Le projet modifie/abroge une disposition modificative qui
est entrée en vigueur :il     faut modifier le texte de base et non la
disposition modificative.''',
       'no' : u'''Le projet modifie/abroge une disposition modificative
qui n’est pas entrée en        vigueur : il faut prévoir une entrée en
vigueur le même jour que la première disposition   modificative.'''
      },

    'abroges_identifies_analyse': \
      {'yes': '',
       'no' : u'''Les actes suivants, que vous abrogez, ne sont pas identifiés avec
précision .'''
      },

    'abroges_cites_analyse': \
      {'yes': '',
       'no' : u'''Les actes abrogés doivent être cités  sous la forme d’une énumération
verticale, en utilisant les subdivisions « 1° », « 2° », « 3° ».'''
      },

    'abroges_pertinentes_analyse': \
      {'yes': '',
       'no' : u'''L’acte à abroger a subi des modifications : abrogez l’acte initial en
citant les modifications dont il a fait l’objet et qui sont toujours
pertinentes.'''
      },

    'abroges_region_analyse': \
      {'yes': u'''Lorsqu’à la suite d’un transfert de compétence, un acte de la Région
abroge un acte de l’Autorité fédérale ou un acte d’une autre entité
fédérée, ne précisez pas « la loi du …est abrogée pour la Région
wallonne ».''',
       'no' : ''
      },

    'abroge_finales_analyse': \
      {'yes': '',
       'no' : u'''Placez la disposition qui abroge totalement un ou plusieurs actes parmi les
dispositions finales, après les dispositions modificatives et avant les
dispositions transitoires, l’entrée en vigueur et l’article d’exécution
(exécutoire).'''
      },

    'abroge_decret_analyse': \
      {'yes': '',
       'no' : u'''Il y a lieu d’abroger les arrêtés d’exécution suivants :    …..'''
      },

    'transitoires_analyse': \
      {'yes': u'''''',
       'no' : ''
      },

    'transitoires_analyse_permanent_analyse': \
      {'yes': u'''L'article (..) a un caractère permanent: il ne peut être placé avec les
dispositions éventuelles transitoires.''',
       'no' : ''
      },

    'transitoires_analyse_temporaire_analyse': \
      {'yes': u'''L'article (..) a un caractère temporaire : il ne peut être placé dans les
éventuelles dispositions transitoires.''',
       'no' : ''
      },

    'entree_analyse': \
      {'yes': '',
       'no' : u'''Le projet ne prévoit pas de date d’entrée en vigueur : l’acte entrera en
vigueur le dixième jour qui suit sa publication au Moniteur belge.'''
      },

    'entree_analyse': \
      {'yes': u'''''',
       'no' : ''
      },

    'entree_analyse_delai_analyse': \
      {'yes': '',
       'no' : u'''Le projet déroge, sans raisons impérieuses, au délai de 10 jours. Il y a
lieu de retirer la date d'entrée en vigueur'''
      },

    'entree_analyse_retroactive_analyse': \
      {'yes': u'''Vous prévoyez que l’acte s’appliquera à une
date antérieure à sa promulgation/publication : examinez l’admissibilité de
la rétroactivité.''',
       'no' : ''
      },

    'entree_analyse_informe_analyse': \
      {'yes': u'''Le délai entre la publication et la date d'entrée en
vigueur permet/ne permet pas d'informer les personnes concernées par les
règles nouvelles.''',
       'no' : ''
      },

    'entree_analyse_temps_analyse': \
      {'yes': u'''Le délai entre la publication et la date
d’entrée en vigueur permet/ne permet pas aux personnes tenues d’obligations
nouvelles de s’y conformer.''',
       'no' : ''
      },

    'entree_analyse_mesures_materielles_analyse': \
      {'yes': u'''Le délai entre la publication et la date
d’entrée en vigueur permet/ne permet pas aux services chargés de
l’application de l’acte de prendre les mesures préalables d’exécution.''',
       'no' : ''
      },

    'entree_analyse_date_entree_analyse': \
      {'yes': u'''''',
       'no' : ''
      },

    'entree_analyse_date_entree_analyse_ultime_analyse': \
      {'yes': '',
       'no' : u'''Il y a lieu de prévoir une date ultime d'entrée en vigueur de
l'acte législatif.'''
      },

    'entree_analyse_preambule_analyse': \
      {'yes': '',
       'no' : u'''Dans le préambule de l'arrêté, indiquez son fondement juridique en
visant l'article de l'acte législatif d'habilitation.'''
      },

    'entree_analyse_a_la_fin_analyse': \
      {'yes': '',
       'no' : u'''La disposition fixant l'entrée en vigueur ne se trouve pas au bon
endroit : placez-la à la fin de l'acte, avant l'article d'exécution.'''
      },

    'ministre_designe_analyse': \
      {'yes': u'''L'exécution de l'arrêté doit être assurée au-delà des changements de
Ministre. Dès lors, désignez de façon abstraite le Ministre qui est
compétent dans la matière réglée par l'arrêté.
Exemple : "Le ministre qui a l'environnement dans ses attributions est
chargé de l'exécution du présent arrêté.''',
       'no' : ''
      },

    'annexe_analyse': \
      {'yes': '',
       'no' : u'''A l'annexe, inscrivez le mot "annexe" dans son en-tête'''
      },

    'annexe_intitule_analyse': \
      {'yes': '',
       'no' : u'''L'intitulé de l'annexe n'est pas précis/complet/concis. L'intitulé
suivant est proposé:'''
      },

    'annexe_droits_analyse': \
      {'yes': u'''Les articles ……. énoncent des droits et obligations autres que
ceux mentionnés dans le dispositif : il faut les inscrire dans le
dispositif.''',
       'no' : ''
      },

    'annexe_mention_analyse': \
      {'yes': '',
       'no' : u'''Vous devez mentionner la formule suivante  à la fin de l'annexe: "Vu
pour être annexé à l'arrêté du Gouvernement wallon du (date et intitulé)'''
      },

    'annexe_signatures_analyse': \
      {'yes': '',
       'no' : u'''Vous devez soumettre l'annexe aux mêmes signatures que le texte auquel
elle est jointe.'''
      },

    'phrases_simples_analyse': \
      {'yes': '',
       'no' : u'''...'''
      },

    'repartition_competences_analyse': \
      {'yes': u'''...''',
       'no' : ''
      },

    'libertes_publiques_analyse': \
      {'yes': u'''...''',
       'no' : ''
      },

    'droit_europeen_analyse': \
      {'yes': u'''...''',
       'no' : ''
      },
}


schema = Schema((

    # Attributs liés au texte en projet ----------------------------------------

    StringField(
        name='apType',
        widget=SelectionWidget(
            label="Type de norme",
            format="select",
            label_msgid='Avis_label_apType',
            i18n_domain='Avis',
        ),
        enforceVocabulary=True,
        vocabulary=DisplayList(( ('arreteGouv', u'arrêté du Gouvernement wallon'),
                                 ('arreteMin', u'arrêté ministériel'),
                                 ('decret', u'décret') )),
        required=True
    ),

    StringField(
        name='apTitle',
        widget=TextAreaWidget(
            label=u"Intitulé de la norme",
            rows=3,
            label_msgid='Avis_label_apTitle',
            i18n_domain='Avis',
        ),
        required=True
    ),

    StringField(
        name='apDescription',
        widget=TextAreaWidget(
            label=u"Contexte",
            rows=5,
            label_msgid='Avis_label_apDescription',
            i18n_domain='Avis',
        )
    ),

    # Q and A

    StringField(
        name='directive',
        widget=MasterSelectWidget(
            label=u"""Le projet vise-t-il à transposer une directive européenne ?""",
            slave_fields = 
                     slaveFields('showIfYes', 'directive_analyse_article1', 'directive_analyse_alteration', 'directive_analyse_reference', 'directive_analyse_litteralement', 'directive_analyse_particuliers', 'directive_analyse_regionales', 'directive_analyse_circulaire'),
            label_msgid='AvisLegis_directive',
            i18n_domain='Avis',
        ),
        enforceVocabulary=True,
        required=True,
        vocabulary='getResponses'
    ),


    StringField(
        name='directive_analyse_article1',
        widget=MasterSelectWidget(
            label=u"""L’article 1er est-il rédigé comme suit :
« Article 1er. Le présent décret/arrêté transpose ( partiellement) la
directive … ».""",
            slave_fields = 
                     slaveFields('showIfNo', 'directive_analyse_article1_analyse'),
            label_msgid='AvisLegis_directive_analyse_article1',
            i18n_domain='Avis',
        ),
        enforceVocabulary=True,
        required=True,
        vocabulary='getResponsesInversed'
    ),


    TextField(
        name='directive_analyse_article1_analyse',
        widget=TextAreaWidget(
            label=u"Analyse",
            label_msgid='Avis_label_directive_analyse_article1_analyse',
            i18n_domain='Avis',
        ),  
        default_output_type="text/html",
        default=defaultAnalysisValues['directive_analyse_article1_analyse']['no']
    ),  


    StringField(
        name='directive_analyse_alteration',
        widget=MasterSelectWidget(
            label=u"""Les définitions de la directive  sont-elles intégrées sans altération de
leur contenu ?""",
            slave_fields = slaveFields('showIfNo', 'directive_analyse_alteration_analyse'),
            label_msgid='AvisLegis_directive_analyse_alteration',
            i18n_domain='Avis',
        ),
        enforceVocabulary=True,
        required=True,
        vocabulary='getResponsesInversed'
    ),

    TextField(
        name='directive_analyse_alteration_analyse',
        widget=TextAreaWidget(
            label=u"Analyse",
            label_msgid='Avis_label_directive_analyse_alteration_analyse',
            i18n_domain='Avis',
        ),  
        default_output_type="text/html",
        default=defaultAnalysisValues['directive_analyse_alteration_analyse']['no']
    ),  


    StringField(
        name='directive_analyse_reference',
        widget=MasterSelectWidget(
            label=u"""Le projet transpose-t –il par référence ?""",
            slave_fields = 
                     slaveFields('showIfYes', 'directive_analyse_reference_analyse'),
            label_msgid='AvisLegis_directive_analyse_reference',
            i18n_domain='Avis',
        ),
        enforceVocabulary=True,
        required=True,
        vocabulary='getResponses'
    ),


    TextField(
        name='directive_analyse_reference_analyse',
        widget=TextAreaWidget(
            label=u"Analyse",
            label_msgid='Avis_label_directive_analyse_reference_analyse',
            i18n_domain='Avis',
        ),  
        default_output_type="text/html",
        default=defaultAnalysisValues['directive_analyse_reference_analyse']['yes']
    ),  


    StringField(
        name='directive_analyse_litteralement',
        widget=MasterSelectWidget(
            label=u"""Le projet recopie- t il littéralement la Directive ?""",
            slave_fields = 
                     slaveFields('showIfYes', 'directive_analyse_litteralement_analyse'),
            label_msgid='AvisLegis_directive_analyse_litteralement',
            i18n_domain='Avis',
        ),
        enforceVocabulary=True,
        required=True,
        vocabulary='getResponses'
    ),


    TextField(
        name='directive_analyse_litteralement_analyse',
        widget=TextAreaWidget(
            label=u"Analyse",
            label_msgid='Avis_label_directive_analyse_litteralement_analyse',
            i18n_domain='Avis',
        ),  
        default_output_type="text/html",
        default=defaultAnalysisValues['directive_analyse_litteralement_analyse']['yes']
    ),  


    StringField(
        name='directive_analyse_particuliers',
        widget=MasterSelectWidget(
            label=u"""Les dispositions transposées concernent-elles les particuliers ?""",
            slave_fields = 
                     slaveFields('showIfNo', 'directive_analyse_particuliers_analyse'),
            label_msgid='AvisLegis_directive_analyse_particuliers',
            i18n_domain='Avis',
        ),
        enforceVocabulary=True,
        required=True,
        vocabulary='getResponsesInversed'
    ),


    TextField(
        name='directive_analyse_particuliers_analyse',
        widget=TextAreaWidget(
            label=u"Analyse",
            label_msgid='Avis_label_directive_analyse_particuliers_analyse',
            i18n_domain='Avis',
        ),  
        default_output_type="text/html",
        default=defaultAnalysisValues['directive_analyse_particuliers_analyse']['no']
    ),  


    StringField(
        name='directive_analyse_regionales',
        widget=MasterSelectWidget(
            label=u"""Le projet rentre-t-il intégralement dans les compétences régionales ?""",
            slave_fields = 
                     slaveFields('showIfNo', 'directive_analyse_regionales_analyse'),
            label_msgid='AvisLegis_directive_analyse_regionales',
            i18n_domain='Avis',
        ),
        enforceVocabulary=True,
        required=True,
        vocabulary='getResponsesInversed'
    ),


    TextField(
        name='directive_analyse_regionales_analyse',
        widget=TextAreaWidget(
            label=u"Analyse",
            label_msgid='Avis_label_directive_analyse_regionales_analyse',
            i18n_domain='Avis',
        ),  
        default_output_type="text/html",
        default=defaultAnalysisValues['directive_analyse_regionales_analyse']['no']
    ),  


    StringField(
        name='directive_analyse_circulaire',
        widget=MasterSelectWidget(
            label=u"""La directive est-elle transposée par une circulaire ?""",
            slave_fields = 
                     slaveFields('showIfYes', 'directive_analyse_circulaire_analyse'),
            label_msgid='AvisLegis_directive_analyse_circulaire',
            i18n_domain='Avis',
        ),
        enforceVocabulary=True,
        required=True,
        vocabulary='getResponses'
    ),


    TextField(
        name='directive_analyse_circulaire_analyse',
        widget=TextAreaWidget(
            label=u"Analyse",
            label_msgid='Avis_label_directive_analyse_circulaire_analyse',
            i18n_domain='Avis',
        ),  
        default_output_type="text/html",
        default=defaultAnalysisValues['directive_analyse_circulaire_analyse']['yes']
    ),  


    StringField(
        name='traite',
        widget=MasterSelectWidget(
            label=u"""Le projet porte-t-il assentiment à un traité ?""",
            slave_fields = 
                     slaveFields('showIfYes', 'traite_analyse_constitution', 'traite_analyse_plusieurs'),
            label_msgid='AvisLegis_traite',
            i18n_domain='Avis',
        ),
        enforceVocabulary=True,
        required=True,
        vocabulary='getResponses'
    ),


    StringField(
        name='traite_analyse_constitution',
        widget=MasterSelectWidget(
            label=u"""L’avant-projet respecte-t-il les dispositions de la Constitution ?""",
            slave_fields = 
                     slaveFields('showIfNo', 'traite_analyse_constitution_analyse'),
            label_msgid='AvisLegis_traite_analyse_constitution',
            i18n_domain='Avis',
        ),
        enforceVocabulary=True,
        required=True,
        vocabulary='getResponsesInversed'
    ),


    TextField(
        name='traite_analyse_constitution_analyse',
        widget=TextAreaWidget(
            label=u"Analyse",
            label_msgid='Avis_label_traite_analyse_constitution_analyse',
            i18n_domain='Avis',
        ),  
        default_output_type="text/html",
        default=defaultAnalysisValues['traite_analyse_constitution_analyse']['no']
    ),  


    StringField(
        name='traite_analyse_plusieurs',
        widget=MasterSelectWidget(
            label=u"""L’avant-projet porte-t-il assentiment à plusieurs traités ?""",
            slave_fields = 
                     slaveFields('showIfYes', 'traite_analyse_plusieurs_analyse_distincts'),
            label_msgid='AvisLegis_traite_analyse_plusieurs',
            i18n_domain='Avis',
        ),
        enforceVocabulary=True,
        required=True,
        vocabulary='getResponses'
    ),


    StringField(
        name='traite_analyse_plusieurs_analyse_distincts',
        widget=MasterSelectWidget(
            label=u"""Un article distinct est-il rédigé pour chaque traîté ?""",
            slave_fields = 
                     slaveFields('showIfNo', 'traite_analyse_plusieurs_analyse_distincts_analyse'),
            label_msgid='AvisLegis_traite_analyse_plusieurs_analyse_distincts',
            i18n_domain='Avis',
        ),
        enforceVocabulary=True,
        required=True,
        vocabulary='getResponsesInversed'
    ),


    TextField(
        name='traite_analyse_plusieurs_analyse_distincts_analyse',
        widget=TextAreaWidget(
            label=u"Analyse",
            label_msgid='Avis_label_traite_analyse_plusieurs_analyse_distincts_analyse',
            i18n_domain='Avis',
        ),  
        default_output_type="text/html",
        default=defaultAnalysisValues['traite_analyse_plusieurs_analyse_distincts_analyse']['no']
    ),  


    StringField(
        name='cooperation',
        widget=MasterSelectWidget(
            label=u"""Le projet porte-t-il assentiment à un accord de coopération au sens de
l’article 92 bis de la loi spéciale du 8 août 1980 de réformes
institutionnelles ?""",
            slave_fields = 
                     slaveFields('showIfYes', 'cooperation_analyse_parties'),
            label_msgid='AvisLegis_cooperation',
            i18n_domain='Avis',
        ),
        enforceVocabulary=True,
        required=True,
        vocabulary='getResponses'
    ),


    StringField(
        name='cooperation_analyse_parties',
        widget=MasterSelectWidget(
            label=u"""L’accord a-t-il été signé par toutes les parties ?""",
            slave_fields = 
                     slaveFields('showIfNo', 'cooperation_analyse_parties_analyse'),
            label_msgid='AvisLegis_cooperation_analyse_parties',
            i18n_domain='Avis',
        ),
        enforceVocabulary=True,
        required=True,
        vocabulary='getResponsesInversed'
    ),


    TextField(
        name='cooperation_analyse_parties_analyse',
        widget=TextAreaWidget(
            label=u"Analyse",
            label_msgid='Avis_label_cooperation_analyse_parties_analyse',
            i18n_domain='Avis',
        ),  
        default_output_type="text/html",
        default=defaultAnalysisValues['cooperation_analyse_parties_analyse']['no']
    ),  



    StringField(
        name='precis',
        widget=MasterSelectWidget(
            label=u"""L’intitulé est-il précis, complet et concis ?""",
            slave_fields = 
                     slaveFields('showIfNo', 'precis_analyse'),
            label_msgid='AvisLegis_precis',
            i18n_domain='Avis',
        ),
        enforceVocabulary=True,
        required=True,
        vocabulary='getResponsesInversed'
    ),


    TextField(
        name='precis_analyse',
        widget=TextAreaWidget(
            label=u"Analyse",
            label_msgid='Avis_label_precis_analyse',
            i18n_domain='Avis',
        ),  
        default_output_type="text/html",
        default=defaultAnalysisValues['precis_analyse']['no']
    ),  



    StringField(
        name='decret',
        widget=MasterSelectWidget(
            label=u"""S’agit-il d’un projet de décret ?""",
            slave_fields = 
                     slaveFields('showIfYes', 'decret_analyse_presentation') + \
                     slaveFields('showIfYes', 'decret_analyse_presentation_analyse'),
            label_msgid='AvisLegis_decret',
            i18n_domain='Avis',
        ),
        enforceVocabulary=True,
        required=True,
        vocabulary='getResponses'
    ),

    StringField(
        name='decret_analyse_presentation',
        widget=MasterSelectWidget(
            label=u"""Y a-t-il un arrêté de présentation ?""",
            slave_fields = (
                {'name': 'decret_analyse_presentation_analyse',
                 'action': 'value',
                 'vocab_method': 'get_decretPresentationAnalyse',
                 'control_param': 'param'},
                ),
            label_msgid='AvisLegis_decret_analyse_presentation',
            i18n_domain='Avis',
        ),
        enforceVocabulary=True,
        required=True,
        vocabulary=[u'Oui', u'Oui, mais mal rédigé', 'Non']
    ),

    TextField(
        name='decret_analyse_presentation_analyse',
        widget=TextAreaWidget(
            label=u"Analyse",
            label_msgid='Avis_label_decret_analyse_presentation_analyse',
            i18n_domain='Avis',
        ),  
        default_output_type="text/html",
    ),

    StringField(
        name='debut',
        widget=MasterSelectWidget(
            label=u"""la formule de début ?""",
            slave_fields = 
                     slaveFields('showIfNo', 'debut_analyse'),
            label_msgid='AvisLegis_debut',
            i18n_domain='Avis',
        ),
        enforceVocabulary=True,
        required=True,
        vocabulary='getResponsesInversed'
    ),


    TextField(
        name='debut_analyse',
        widget=TextAreaWidget(
            label=u"Analyse",
            label_msgid='Avis_label_debut_analyse',
            i18n_domain='Avis',
        ),  
        default_output_type="text/html",
        default=defaultAnalysisValues['debut_analyse']['no']
    ),  


    StringField(
        name='reglement',
        widget=MasterSelectWidget(
            label=u"""tout règlement européen qui est exécuté en droit interne par l’arrêté ?""",
            slave_fields = 
                     slaveFields('showIfNo', 'reglement_analyse'),
            label_msgid='AvisLegis_reglement',
            i18n_domain='Avis',
        ),
        enforceVocabulary=True,
        required=True,
        vocabulary='getResponsesInversed'
    ),


    TextField(
        name='reglement_analyse',
        widget=TextAreaWidget(
            label=u"Analyse",
            label_msgid='Avis_label_reglement_analyse',
            i18n_domain='Avis',
        ),  
        default_output_type="text/html",
        default=defaultAnalysisValues['reglement_analyse']['no']
    ),  


    StringField(
        name='actes_fondement',
        widget=MasterSelectWidget(
            label=u"""les actes de droit interne et les articles qui constituent le fondement
juridique de l’arrêté ?""",
            slave_fields = 
                     slaveFields('showIfNo', 'actes_fondement_analyse'),
            label_msgid='AvisLegis_actes_fondement',
            i18n_domain='Avis',
        ),
        enforceVocabulary=True,
        required=True,
        vocabulary='getResponsesInversed'
    ),


    TextField(
        name='actes_fondement_analyse',
        widget=TextAreaWidget(
            label=u"Analyse",
            label_msgid='Avis_label_actes_fondement_analyse',
            i18n_domain='Avis',
        ),  
        default_output_type="text/html",
        default=defaultAnalysisValues['actes_fondement_analyse']['no']
    ),  


    StringField(
        name='actes_modifies',
        widget=MasterSelectWidget(
            label=u"""les actes de droit interne qui sont modifiés, abrogés ou retirés par
l’arrêté ?""",
            slave_fields = 
                     slaveFields('showIfNo', 'actes_modifies_analyse'),
            label_msgid='AvisLegis_actes_modifies',
            i18n_domain='Avis',
        ),
        enforceVocabulary=True,
        required=True,
        vocabulary='getResponsesInversed'
    ),


    TextField(
        name='actes_modifies_analyse',
        widget=TextAreaWidget(
            label=u"Analyse",
            label_msgid='Avis_label_actes_modifies_analyse',
            i18n_domain='Avis',
        ),  
        default_output_type="text/html",
        default=defaultAnalysisValues['actes_modifies_analyse']['no']
    ),  


    StringField(
        name='formalites',
        widget=MasterSelectWidget(
            label=u"""les formalités obligatoires accomplies lors de l’élaboration de
l’arrêté ou les raisons de leur inaccomplissement ?""",
            slave_fields = 
                     slaveFields('showIfNo', 'formalites_analyse'),
            label_msgid='AvisLegis_formalites',
            i18n_domain='Avis',
        ),
        enforceVocabulary=True,
        required=True,
        vocabulary='getResponsesInversed'
    ),


    TextField(
        name='formalites_analyse',
        widget=TextAreaWidget(
            label=u"Analyse",
            label_msgid='Avis_label_formalites_analyse',
            i18n_domain='Avis',
        ),  
        default_output_type="text/html",
        default=defaultAnalysisValues['formalites_analyse']['no']
    ),  


    StringField(
        name='ministres',
        widget=MasterSelectWidget(
            label=u"""les Ministres qui proposent l’arrêté ainsi que la délibération du
Gouvernement ?""",
            slave_fields = 
                     slaveFields('showIfNo', 'ministres_analyse'),
            label_msgid='AvisLegis_ministres',
            i18n_domain='Avis',
        ),
        enforceVocabulary=True,
        required=True,
        vocabulary='getResponsesInversed'
    ),


    TextField(
        name='ministres_analyse',
        widget=TextAreaWidget(
            label=u"Analyse",
            label_msgid='Avis_label_ministres_analyse',
            i18n_domain='Avis',
        ),  
        default_output_type="text/html",
        default=defaultAnalysisValues['ministres_analyse']['no']
    ),  


    StringField(
        name='formalites_non_obligatoires',
        widget=MasterSelectWidget(
            label=u"""les formalités non obligatoires accomplies lors de l’élaboration de
l’arrêté ?""",
            slave_fields = 
                     slaveFields('showIfNo', 'formalites_non_obligatoires_analyse'),
            label_msgid='AvisLegis_formalites_non_obligatoires',
            i18n_domain='Avis',
        ),
        enforceVocabulary=True,
        required=True,
        vocabulary='getResponsesInversed'
    ),


    TextField(
        name='formalites_non_obligatoires_analyse',
        widget=TextAreaWidget(
            label=u"Analyse",
            label_msgid='Avis_label_formalites_non_obligatoires_analyse',
            i18n_domain='Avis',
        ),  
        default_output_type="text/html",
        default=defaultAnalysisValues['formalites_non_obligatoires_analyse']['no']
    ),  


    StringField(
        name='justification_arrete',
        widget=MasterSelectWidget(
            label=u"""la justification de la prise de l’arrêté, lorsque la motivation formelle
de l’arrêté n’est pas imposée par une norme ?""",
            slave_fields = 
                     slaveFields('showIfNo', 'justification_arrete_analyse'),
            label_msgid='AvisLegis_justification_arrete',
            i18n_domain='Avis',
        ),
        enforceVocabulary=True,
        required=True,
        vocabulary='getResponsesInversed'
    ),


    TextField(
        name='justification_arrete_analyse',
        widget=TextAreaWidget(
            label=u"Analyse",
            label_msgid='Avis_label_justification_arrete_analyse',
            i18n_domain='Avis',
        ),  
        default_output_type="text/html",
        default=defaultAnalysisValues['justification_arrete_analyse']['no']
    ),  


    StringField(
        name='mot_final',
        widget=MasterSelectWidget(
            label=u"""le mot final ?""",
            slave_fields = 
                     slaveFields('showIfNo', 'mot_final_analyse'),
            label_msgid='AvisLegis_mot_final',
            i18n_domain='Avis',
        ),
        enforceVocabulary=True,
        required=True,
        vocabulary='getResponsesInversed'
    ),


    TextField(
        name='mot_final_analyse',
        widget=TextAreaWidget(
            label=u"Analyse",
            label_msgid='Avis_label_mot_final_analyse',
            i18n_domain='Avis',
        ),  
        default_output_type="text/html",
        default=defaultAnalysisValues['mot_final_analyse']['no']
    ),  


    StringField(
        name='norme_particuliere',
        widget=MasterSelectWidget(
            label=u"""Y a t-il une norme  particulière qui impose une motivation formelle
pour cet arrêté ?""",
            slave_fields = 
                     slaveFields('showIfYes', 'norme_particuliere_analyse'),
            label_msgid='AvisLegis_norme_particuliere',
            i18n_domain='Avis',
        ),
        enforceVocabulary=True,
        required=True,
        vocabulary='getResponses'
    ),


    TextField(
        name='norme_particuliere_analyse',
        widget=TextAreaWidget(
            label=u"Analyse",
            label_msgid='Avis_label_norme_particuliere_analyse',
            i18n_domain='Avis',
        ),  
        default_output_type="text/html",
        default=defaultAnalysisValues['norme_particuliere_analyse']['yes']
    ),  




    StringField(
        name='unique',
        widget=MasterSelectWidget(
            label=u"""Le texte comporte t -il un article unique ?""",
            slave_fields = 
                     slaveFields('showIfYes', 'unique_analyse_correctement_indique') + \
                     slaveFields('showIfNo', 'unique_analyse_numerotation', 'unique_analyse_intitule', 'unique_analyse_division', 'unique_analyse_numerotation_phrase', 'unique_analyse_phrases_incidentes', 'unique_analyse_groupes_articles', 'unique_analyse_une_division', 'unique_analyse_chiffres_arabes', 'unique_analyse_intitule_division', 'unique_analyse_references', 'unique_analyse_reiteratives', 'unique_analyse_sans_portee', 'unique_analyse_portee_individuelle', 'unique_analyse_parenthses', 'unique_analyse_nombres', 'unique_analyse_sigle', 'unique_analyse_euros', 'unique_analyse_majuscule'),
            label_msgid='AvisLegis_unique',
            i18n_domain='Avis',
        ),
        enforceVocabulary=True,
        required=True,
        vocabulary='getResponses'
    ),


    StringField(
        name='unique_analyse_correctement_indique',
        widget=MasterSelectWidget(
            label=u"""L’article est-il correctement indiqué ?""",
            slave_fields = 
                     slaveFields('showIfNo', 'unique_analyse_correctement_indique_analyse'),
            label_msgid='AvisLegis_unique_analyse_correctement_indique',
            i18n_domain='Avis',
        ),
        enforceVocabulary=True,
        required=True,
        vocabulary='getResponsesInversed'
    ),


    TextField(
        name='unique_analyse_correctement_indique_analyse',
        widget=TextAreaWidget(
            label=u"Analyse",
            label_msgid='Avis_label_unique_analyse_correctement_indique_analyse',
            i18n_domain='Avis',
        ),  
        default_output_type="text/html",
        default=defaultAnalysisValues['unique_analyse_correctement_indique_analyse']['no']
    ),  


    StringField(
        name='unique_analyse_numerotation',
        widget=MasterSelectWidget(
            label=u"""Les articles sont-ils indiqués et numérotés de la manière suivante, et
de façon continue du début à la fin du dispositif ?
« Article 1er .», « Art. 2. », « Art. 3. »,…""",
            slave_fields = 
                     slaveFields('showIfNo', 'unique_analyse_numerotation_analyse'),
            label_msgid='AvisLegis_unique_analyse_numerotation',
            i18n_domain='Avis',
        ),
        enforceVocabulary=True,
        required=True,
        vocabulary='getResponsesInversed'
    ),


    TextField(
        name='unique_analyse_numerotation_analyse',
        widget=TextAreaWidget(
            label=u"Analyse",
            label_msgid='Avis_label_unique_analyse_numerotation_analyse',
            i18n_domain='Avis',
        ),  
        default_output_type="text/html",
        default=defaultAnalysisValues['unique_analyse_numerotation_analyse']['no']
    ),  


    StringField(
        name='unique_analyse_intitule',
        widget=MasterSelectWidget(
            label=u"""Y a t-il un intitulé à chaque article ?""",
            slave_fields = 
                     slaveFields('showIfYes', 'unique_analyse_intitule_analyse_specifique', 'unique_analyse_intitule_analyse_fidele'),
            label_msgid='AvisLegis_unique_analyse_intitule',
            i18n_domain='Avis',
        ),
        enforceVocabulary=True,
        required=True,
        vocabulary='getResponses'
    ),


    StringField(
        name='unique_analyse_intitule_analyse_specifique',
        widget=MasterSelectWidget(
            label=u"""Y a t-il un intitulé spécifique pour chaque article ?""",
            slave_fields = 
                     slaveFields('showIfNo', 'unique_analyse_intitule_analyse_specifique_analyse'),
            label_msgid='AvisLegis_unique_analyse_intitule_analyse_specifique',
            i18n_domain='Avis',
        ),
        enforceVocabulary=True,
        required=True,
        vocabulary='getResponsesInversed'
    ),


    TextField(
        name='unique_analyse_intitule_analyse_specifique_analyse',
        widget=TextAreaWidget(
            label=u"Analyse",
            label_msgid='Avis_label_unique_analyse_intitule_analyse_specifique_analyse',
            i18n_domain='Avis',
        ),  
        default_output_type="text/html",
        default=defaultAnalysisValues['unique_analyse_intitule_analyse_specifique_analyse']['no']
    ),  


    StringField(
        name='unique_analyse_intitule_analyse_fidele',
        widget=MasterSelectWidget(
            label=u"""L’intitulé reflète t-il fidèlement et complètement le contenu de
l’article ?""",
            slave_fields = 
                     slaveFields('showIfNo', 'unique_analyse_intitule_analyse_fidele_analyse'),
            label_msgid='AvisLegis_unique_analyse_intitule_analyse_fidele',
            i18n_domain='Avis',
        ),
        enforceVocabulary=True,
        required=True,
        vocabulary='getResponsesInversed'
    ),


    TextField(
        name='unique_analyse_intitule_analyse_fidele_analyse',
        widget=TextAreaWidget(
            label=u"Analyse",
            label_msgid='Avis_label_unique_analyse_intitule_analyse_fidele_analyse',
            i18n_domain='Avis',
        ),  
        default_output_type="text/html",
        default=defaultAnalysisValues['unique_analyse_intitule_analyse_fidele_analyse']['no']
    ),  


    StringField(
        name='unique_analyse_division',
        widget=MasterSelectWidget(
            label=u"""La division des articles en alinéas et paragraphes est-elle adéquate ?""",
            slave_fields = 
                     slaveFields('showIfNo', 'unique_analyse_division_analyse'),
            label_msgid='AvisLegis_unique_analyse_division',
            i18n_domain='Avis',
        ),
        enforceVocabulary=True,
        required=True,
        vocabulary='getResponsesInversed'
    ),


    TextField(
        name='unique_analyse_division_analyse',
        widget=TextAreaWidget(
            label=u"Analyse",
            label_msgid='Avis_label_unique_analyse_division_analyse',
            i18n_domain='Avis',
        ),  
        default_output_type="text/html",
        default=defaultAnalysisValues['unique_analyse_division_analyse']['no']
    ),  


    StringField(
        name='unique_analyse_numerotation_phrase',
        widget=MasterSelectWidget(
            label=u"""Les énumérations à l’intérieur d’une phrase sont-elles présentées de la
manière suivante :
« 1° », « 2° », « 3° »,…puis éventuellement « a) », « b) », « c) »,… ?""",
            slave_fields = 
                     slaveFields('showIfNo', 'unique_analyse_numerotation_phrase_analyse'),
            label_msgid='AvisLegis_unique_analyse_numerotation_phrase',
            i18n_domain='Avis',
        ),
        enforceVocabulary=True,
        required=True,
        vocabulary='getResponsesInversed'
    ),


    TextField(
        name='unique_analyse_numerotation_phrase_analyse',
        widget=TextAreaWidget(
            label=u"Analyse",
            label_msgid='Avis_label_unique_analyse_numerotation_phrase_analyse',
            i18n_domain='Avis',
        ),  
        default_output_type="text/html",
        default=defaultAnalysisValues['unique_analyse_numerotation_phrase_analyse']['no']
    ),  


    StringField(
        name='unique_analyse_phrases_incidentes',
        widget=MasterSelectWidget(
            label=u"""Y a t-il des phrases incidentes dans l’énumération ?""",
            slave_fields = 
                     slaveFields('showIfYes', 'unique_analyse_phrases_incidentes_analyse'),
            label_msgid='AvisLegis_unique_analyse_phrases_incidentes',
            i18n_domain='Avis',
        ),
        enforceVocabulary=True,
        required=True,
        vocabulary='getResponses'
    ),


    TextField(
        name='unique_analyse_phrases_incidentes_analyse',
        widget=TextAreaWidget(
            label=u"Analyse",
            label_msgid='Avis_label_unique_analyse_phrases_incidentes_analyse',
            i18n_domain='Avis',
        ),  
        default_output_type="text/html",
        default=defaultAnalysisValues['unique_analyse_phrases_incidentes_analyse']['yes']
    ),  


    StringField(
        name='unique_analyse_groupes_articles',
        widget=MasterSelectWidget(
            label=u"""Les articles sont-ils groupés de la façon suivante :Partie,  Livre, Titre,
Chapitre, Section, Sous-section ?""",
            slave_fields = 
                     slaveFields('showIfNo', 'unique_analyse_groupes_articles_analyse'),
            label_msgid='AvisLegis_unique_analyse_groupes_articles',
            i18n_domain='Avis',
        ),
        enforceVocabulary=True,
        required=True,
        vocabulary='getResponsesInversed'
    ),


    TextField(
        name='unique_analyse_groupes_articles_analyse',
        widget=TextAreaWidget(
            label=u"Analyse",
            label_msgid='Avis_label_unique_analyse_groupes_articles_analyse',
            i18n_domain='Avis',
        ),  
        default_output_type="text/html",
        default=defaultAnalysisValues['unique_analyse_groupes_articles_analyse']['no']
    ),  


    StringField(
        name='unique_analyse_une_division',
        widget=MasterSelectWidget(
            label=u"""Tous les articles se trouvent-ils dans une division ?""",
            slave_fields = 
                     slaveFields('showIfNo', 'unique_analyse_une_division_analyse'),
            label_msgid='AvisLegis_unique_analyse_une_division',
            i18n_domain='Avis',
        ),
        enforceVocabulary=True,
        required=True,
        vocabulary='getResponsesInversed'
    ),


    TextField(
        name='unique_analyse_une_division_analyse',
        widget=TextAreaWidget(
            label=u"Analyse",
            label_msgid='Avis_label_unique_analyse_une_division_analyse',
            i18n_domain='Avis',
        ),  
        default_output_type="text/html",
        default=defaultAnalysisValues['unique_analyse_une_division_analyse']['no']
    ),  


    StringField(
        name='unique_analyse_chiffres_arabes',
        widget=MasterSelectWidget(
            label=u"""Les divisions sont-elles numérotées en chiffres arabes ?""",
            slave_fields = 
                     slaveFields('showIfNo', 'unique_analyse_chiffres_arabes_analyse'),
            label_msgid='AvisLegis_unique_analyse_chiffres_arabes',
            i18n_domain='Avis',
        ),
        enforceVocabulary=True,
        required=True,
        vocabulary='getResponsesInversed'
    ),


    TextField(
        name='unique_analyse_chiffres_arabes_analyse',
        widget=TextAreaWidget(
            label=u"Analyse",
            label_msgid='Avis_label_unique_analyse_chiffres_arabes_analyse',
            i18n_domain='Avis',
        ),  
        default_output_type="text/html",
        default=defaultAnalysisValues['unique_analyse_chiffres_arabes_analyse']['no']
    ),  


    StringField(
        name='unique_analyse_intitule_division',
        widget=MasterSelectWidget(
            label=u"""Chaque division a t-elle un intitulé précis, complet et concis ?""",
            slave_fields = 
                     slaveFields('showIfNo', 'unique_analyse_intitule_division_analyse'),
            label_msgid='AvisLegis_unique_analyse_intitule_division',
            i18n_domain='Avis',
        ),
        enforceVocabulary=True,
        required=True,
        vocabulary='getResponsesInversed'
    ),


    TextField(
        name='unique_analyse_intitule_division_analyse',
        widget=TextAreaWidget(
            label=u"Analyse",
            label_msgid='Avis_label_unique_analyse_intitule_division_analyse',
            i18n_domain='Avis',
        ),  
        default_output_type="text/html",
        default=defaultAnalysisValues['unique_analyse_intitule_division_analyse']['no']
    ),  


    StringField(
        name='unique_analyse_references',
        widget=MasterSelectWidget(
            label=u"""Les références (internes et externes) sont-elles correctes ?""",
            slave_fields = 
                     slaveFields('showIfNo', 'unique_analyse_references_analyse'),
            label_msgid='AvisLegis_unique_analyse_references',
            i18n_domain='Avis',
        ),
        enforceVocabulary=True,
        required=True,
        vocabulary='getResponsesInversed'
    ),


    TextField(
        name='unique_analyse_references_analyse',
        widget=TextAreaWidget(
            label=u"Analyse",
            label_msgid='Avis_label_unique_analyse_references_analyse',
            i18n_domain='Avis',
        ),  
        default_output_type="text/html",
        default=defaultAnalysisValues['unique_analyse_references_analyse']['no']
    ),  


    StringField(
        name='unique_analyse_reiteratives',
        widget=MasterSelectWidget(
            label=u"""Y a-t-il des dispositions réitératives ?""",
            slave_fields = 
                     slaveFields('showIfYes', 'unique_analyse_reiteratives_analyse'),
            label_msgid='AvisLegis_unique_analyse_reiteratives',
            i18n_domain='Avis',
        ),
        enforceVocabulary=True,
        required=True,
        vocabulary='getResponses'
    ),


    TextField(
        name='unique_analyse_reiteratives_analyse',
        widget=TextAreaWidget(
            label=u"Analyse",
            label_msgid='Avis_label_unique_analyse_reiteratives_analyse',
            i18n_domain='Avis',
        ),  
        default_output_type="text/html",
        default=defaultAnalysisValues['unique_analyse_reiteratives_analyse']['yes']
    ),  


    StringField(
        name='unique_analyse_sans_portee',
        widget=MasterSelectWidget(
            label=u"""Y a-t-il des dispositions sans portée normative ?""",
            slave_fields = 
                     slaveFields('showIfYes', 'unique_analyse_sans_portee_analyse'),
            label_msgid='AvisLegis_unique_analyse_sans_portee',
            i18n_domain='Avis',
        ),
        enforceVocabulary=True,
        required=True,
        vocabulary='getResponses'
    ),


    TextField(
        name='unique_analyse_sans_portee_analyse',
        widget=TextAreaWidget(
            label=u"Analyse",
            label_msgid='Avis_label_unique_analyse_sans_portee_analyse',
            i18n_domain='Avis',
        ),  
        default_output_type="text/html",
        default=defaultAnalysisValues['unique_analyse_sans_portee_analyse']['yes']
    ),  


    StringField(
        name='unique_analyse_portee_individuelle',
        widget=MasterSelectWidget(
            label=u"""Y a-t-il des dispositions à portée individuelle ?""",
            slave_fields = 
                     slaveFields('showIfYes', 'unique_analyse_portee_individuelle_analyse'),
            label_msgid='AvisLegis_unique_analyse_portee_individuelle',
            i18n_domain='Avis',
        ),
        enforceVocabulary=True,
        required=True,
        vocabulary='getResponses'
    ),


    TextField(
        name='unique_analyse_portee_individuelle_analyse',
        widget=TextAreaWidget(
            label=u"Analyse",
            label_msgid='Avis_label_unique_analyse_portee_individuelle_analyse',
            i18n_domain='Avis',
        ),  
        default_output_type="text/html",
        default=defaultAnalysisValues['unique_analyse_portee_individuelle_analyse']['yes']
    ),  


    StringField(
        name='unique_analyse_parenthses',
        widget=MasterSelectWidget(
            label=u"""Y a-t-il des termes entre parenthèses et des notes en bas de page ?""",
            slave_fields = 
                     slaveFields('showIfYes', 'unique_analyse_parenthses_analyse'),
            label_msgid='AvisLegis_unique_analyse_parenthses',
            i18n_domain='Avis',
        ),
        enforceVocabulary=True,
        required=True,
        vocabulary='getResponses'
    ),


    TextField(
        name='unique_analyse_parenthses_analyse',
        widget=TextAreaWidget(
            label=u"Analyse",
            label_msgid='Avis_label_unique_analyse_parenthses_analyse',
            i18n_domain='Avis',
        ),  
        default_output_type="text/html",
        default=defaultAnalysisValues['unique_analyse_parenthses_analyse']['yes']
    ),  


    StringField(
        name='unique_analyse_nombres',
        widget=MasterSelectWidget(
            label=u"""Y a-t-il des nombres cardinaux et ordinaux ?""",
            slave_fields = 
                     slaveFields('showIfYes', 'unique_analyse_nombres_analyse_tres_eleve') + \
                     slaveFields('showIfYes', 'unique_analyse_nombres_analyse_tres_eleve_analyse'),
            label_msgid='AvisLegis_unique_analyse_nombres',
            i18n_domain='Avis',
        ),
        enforceVocabulary=True,
        required=True,
        vocabulary='getResponses'
    ),


    StringField(
        name='unique_analyse_nombres_analyse_tres_eleve',
        widget=MasterSelectWidget(
            label=u"""S’agit-il  d’un nombre très élevé /d’une somme d’argent/ d’une date/de la
référence à un article ?""",
            slave_fields = 
                     slaveFields('setValue', 'unique_analyse_nombres_analyse_tres_eleve_analyse'),
            label_msgid='AvisLegis_unique_analyse_nombres_analyse_tres_eleve',
            i18n_domain='Avis',
        ),
        enforceVocabulary=True,
        required=True,
        vocabulary='getResponses'
    ),


    TextField(
        name='unique_analyse_nombres_analyse_tres_eleve_analyse',
        widget=TextAreaWidget(
            label=u"Analyse",
            label_msgid='Avis_label_unique_analyse_nombres_analyse_tres_eleve_analyse',
            i18n_domain='Avis',
        ),  
        default_output_type="text/html",
        default=defaultAnalysisValues['unique_analyse_nombres_analyse_tres_eleve_analyse']['yes']
    ),

    StringField(
        name='unique_analyse_sigle',
        widget=MasterSelectWidget(
            label=u"""Y a-t-il un sigle d’une institution ?""",
            slave_fields = 
                     slaveFields('showIfYes', 'unique_analyse_sigle_analyse_lettres', 'unique_analyse_sigle_analyse_points'),
            label_msgid='AvisLegis_unique_analyse_sigle',
            i18n_domain='Avis',
        ),
        enforceVocabulary=True,
        required=True,
        vocabulary='getResponses'
    ),


    StringField(
        name='unique_analyse_sigle_analyse_lettres',
        widget=MasterSelectWidget(
            label=u"""Si le sigle de l’institution se prononce lettre par lettre, est-il écrit
en séparant chaque lettre par un point ?""",
            slave_fields = 
                     slaveFields('showIfNo', 'unique_analyse_sigle_analyse_lettres_analyse'),
            label_msgid='AvisLegis_unique_analyse_sigle_analyse_lettres',
            i18n_domain='Avis',
        ),
        enforceVocabulary=True,
        required=True,
        vocabulary='getResponsesInversed'
    ),


    TextField(
        name='unique_analyse_sigle_analyse_lettres_analyse',
        widget=TextAreaWidget(
            label=u"Analyse",
            label_msgid='Avis_label_unique_analyse_sigle_analyse_lettres_analyse',
            i18n_domain='Avis',
        ),  
        default_output_type="text/html",
        default=defaultAnalysisValues['unique_analyse_sigle_analyse_lettres_analyse']['no']
    ),  


    StringField(
        name='unique_analyse_sigle_analyse_points',
        widget=MasterSelectWidget(
            label=u"""Si le sigle se prononce comme un mot ordinaire, est-il écrit sans point
entre les lettres ?""",
            slave_fields = 
                     slaveFields('showIfNo', 'unique_analyse_sigle_analyse_points_analyse'),
            label_msgid='AvisLegis_unique_analyse_sigle_analyse_points',
            i18n_domain='Avis',
        ),
        enforceVocabulary=True,
        required=True,
        vocabulary='getResponsesInversed'
    ),


    TextField(
        name='unique_analyse_sigle_analyse_points_analyse',
        widget=TextAreaWidget(
            label=u"Analyse",
            label_msgid='Avis_label_unique_analyse_sigle_analyse_points_analyse',
            i18n_domain='Avis',
        ),  
        default_output_type="text/html",
        default=defaultAnalysisValues['unique_analyse_sigle_analyse_points_analyse']['no']
    ),  


    StringField(
        name='unique_analyse_euros',
        widget=MasterSelectWidget(
            label=u"""Y a-t-il des montants en euros ?""",
            slave_fields = 
                     slaveFields('showIfYes', 'unique_analyse_euros_analyse_minuscule', 'unique_analyse_euros_analyse_eur', 'unique_analyse_euros_analyse_sigle'),
            label_msgid='AvisLegis_unique_analyse_euros',
            i18n_domain='Avis',
        ),
        enforceVocabulary=True,
        required=True,
        vocabulary='getResponses'
    ),


    StringField(
        name='unique_analyse_euros_analyse_minuscule',
        widget=MasterSelectWidget(
            label=u"""Les mots « euro » et « cent » sont-ils écrits avec une minuscule ?""",
            slave_fields = 
                     slaveFields('showIfNo', 'unique_analyse_euros_analyse_minuscule_analyse'),
            label_msgid='AvisLegis_unique_analyse_euros_analyse_minuscule',
            i18n_domain='Avis',
        ),
        enforceVocabulary=True,
        required=True,
        vocabulary='getResponsesInversed'
    ),


    TextField(
        name='unique_analyse_euros_analyse_minuscule_analyse',
        widget=TextAreaWidget(
            label=u"Analyse",
            label_msgid='Avis_label_unique_analyse_euros_analyse_minuscule_analyse',
            i18n_domain='Avis',
        ),  
        default_output_type="text/html",
        default=defaultAnalysisValues['unique_analyse_euros_analyse_minuscule_analyse']['no']
    ),  


    StringField(
        name='unique_analyse_euros_analyse_eur',
        widget=MasterSelectWidget(
            label=u"""Le mot « EUR » est-il utilisé dans le texte ?""",
            slave_fields = 
                     slaveFields('showIfYes', 'unique_analyse_euros_analyse_eur_analyse'),
            label_msgid='AvisLegis_unique_analyse_euros_analyse_eur',
            i18n_domain='Avis',
        ),
        enforceVocabulary=True,
        required=True,
        vocabulary='getResponses'
    ),


    TextField(
        name='unique_analyse_euros_analyse_eur_analyse',
        widget=TextAreaWidget(
            label=u"Analyse",
            label_msgid='Avis_label_unique_analyse_euros_analyse_eur_analyse',
            i18n_domain='Avis',
        ),  
        default_output_type="text/html",
        default=defaultAnalysisValues['unique_analyse_euros_analyse_eur_analyse']['yes']
    ),  


    StringField(
        name='unique_analyse_euros_analyse_sigle',
        widget=MasterSelectWidget(
            label=u"""Le sigle « € » apparaît-il ?""",
            slave_fields = 
                     slaveFields('showIfYes', 'unique_analyse_euros_analyse_sigle_analyse'),
            label_msgid='AvisLegis_unique_analyse_euros_analyse_sigle',
            i18n_domain='Avis',
        ),
        enforceVocabulary=True,
        required=True,
        vocabulary='getResponses'
    ),


    TextField(
        name='unique_analyse_euros_analyse_sigle_analyse',
        widget=TextAreaWidget(
            label=u"Analyse",
            label_msgid='Avis_label_unique_analyse_euros_analyse_sigle_analyse',
            i18n_domain='Avis',
        ),  
        default_output_type="text/html",
        default=defaultAnalysisValues['unique_analyse_euros_analyse_sigle_analyse']['yes']
    ),  


    StringField(
        name='unique_analyse_majuscule',
        widget=MasterSelectWidget(
            label=u"""L’emploi de la majuscule est-il correct ?""",
            slave_fields = 
                     slaveFields('showIfNo', 'unique_analyse_majuscule_analyse'),
            label_msgid='AvisLegis_unique_analyse_majuscule',
            i18n_domain='Avis',
        ),
        enforceVocabulary=True,
        required=True,
        vocabulary='getResponsesInversed'
    ),


    TextField(
        name='unique_analyse_majuscule_analyse',
        widget=TextAreaWidget(
            label=u"Analyse",
            label_msgid='Avis_label_unique_analyse_majuscule_analyse',
            i18n_domain='Avis',
        ),  
        default_output_type="text/html",
        default=defaultAnalysisValues['unique_analyse_majuscule_analyse']['no']
    ),  



    StringField(
        name='transfert',
        widget=MasterSelectWidget(
            label=u"""Les dispositions décrétales ou réglementaires envisagées concernent-elles
une matière transférée par la Communauté française ?""",
            slave_fields = 
                     slaveFields('showIfYes', 'transfert_analyse_article1'),
            label_msgid='AvisLegis_transfert',
            i18n_domain='Avis',
        ),
        enforceVocabulary=True,
        required=True,
        vocabulary='getResponses'
    ),


    StringField(
        name='transfert_analyse_article1',
        widget=MasterSelectWidget(
            label=u"""L’article 1er est-il rédigé comme suit :
«  Article 1er. Le présent décret (arrêté) règle, en application de
l’article 138 de la Constitution, une matière visée à l’article 127 (à
l’article 128) de celle-ci. » ?
N.B. L’article 127 concerne les matières culturelles.
L’article 128 concerne les matières personnalisables.""",
            slave_fields = 
                     slaveFields('showIfNo', 'transfert_analyse_article1_analyse'),
            label_msgid='AvisLegis_transfert_analyse_article1',
            i18n_domain='Avis',
        ),
        enforceVocabulary=True,
        required=True,
        vocabulary='getResponsesInversed'
    ),


    TextField(
        name='transfert_analyse_article1_analyse',
        widget=TextAreaWidget(
            label=u"Analyse",
            label_msgid='Avis_label_transfert_analyse_article1_analyse',
            i18n_domain='Avis',
        ),  
        default_output_type="text/html",
        default=defaultAnalysisValues['transfert_analyse_article1_analyse']['no']
    ),  


    StringField(
        name='matiere_regionale',
        widget=MasterSelectWidget(
            label=u"""Les dispositions décrétales ou réglementaires envisagées concernent-elles à
la fois une matière régionale et une matière transférée de la Communauté
française ?""",
            slave_fields = 
                     slaveFields('showIfYes', 'matiere_regionale_analyse'),
            label_msgid='AvisLegis_matiere_regionale',
            i18n_domain='Avis',
        ),
        enforceVocabulary=True,
        required=True,
        vocabulary='getResponses'
    ),


    TextField(
        name='matiere_regionale_analyse',
        widget=TextAreaWidget(
            label=u"Analyse",
            label_msgid='Avis_label_matiere_regionale_analyse',
            i18n_domain='Avis',
        ),  
        default_output_type="text/html",
        default=defaultAnalysisValues['matiere_regionale_analyse']['yes']
    ),  



    StringField(
        name='definition_dictionnaire',
        widget=MasterSelectWidget(
            label=u"""La signification exacte des mots définis peut-elle être connue en
consultant le dictionnaire ?""",
            slave_fields = 
                     slaveFields('showIfYes', 'definition_dictionnaire_analyse'),
            label_msgid='AvisLegis_definition_dictionnaire',
            i18n_domain='Avis',
        ),
        enforceVocabulary=True,
        required=True,
        vocabulary='getResponses'
    ),


    TextField(
        name='definition_dictionnaire_analyse',
        widget=TextAreaWidget(
            label=u"Analyse",
            label_msgid='Avis_label_definition_dictionnaire_analyse',
            i18n_domain='Avis',
        ),  
        default_output_type="text/html",
        default=defaultAnalysisValues['definition_dictionnaire_analyse']['yes']
    ),  


    StringField(
        name='definition_acte',
        widget=MasterSelectWidget(
            label=u"""Les définitions ont-elles pour objet de citer un acte, une formule ou le
nom d’une institution sous une forme abrégée ?""",
            slave_fields = 
                     slaveFields('showIfNo', 'definition_acte_analyse'),
            label_msgid='AvisLegis_definition_acte',
            i18n_domain='Avis',
        ),
        enforceVocabulary=True,
        required=True,
        vocabulary='getResponsesInversed'
    ),


    TextField(
        name='definition_acte_analyse',
        widget=TextAreaWidget(
            label=u"Analyse",
            label_msgid='Avis_label_definition_acte_analyse',
            i18n_domain='Avis',
        ),  
        default_output_type="text/html",
        default=defaultAnalysisValues['definition_acte_analyse']['no']
    ),  


    StringField(
        name='definition_acte_superieur',
        widget=MasterSelectWidget(
            label=u"""Les définitions portent-elles sur des mots énoncés par un acte
hiérarchiquement supérieur ?""",
            slave_fields = 
                     slaveFields('showIfYes', 'definition_acte_superieur_analyse'),
            label_msgid='AvisLegis_definition_acte_superieur',
            i18n_domain='Avis',
        ),
        enforceVocabulary=True,
        required=True,
        vocabulary='getResponses'
    ),


    TextField(
        name='definition_acte_superieur_analyse',
        widget=TextAreaWidget(
            label=u"Analyse",
            label_msgid='Avis_label_definition_acte_superieur_analyse',
            i18n_domain='Avis',
        ),  
        default_output_type="text/html",
        default=defaultAnalysisValues['definition_acte_superieur_analyse']['yes']
    ),  


    StringField(
        name='definition_elements',
        widget=MasterSelectWidget(
            label=u"""Les définitions contiennent-elles des éléments normatifs (droits et
obligations) ?""",
            slave_fields = 
                     slaveFields('showIfYes', 'definition_elements_analyse'),
            label_msgid='AvisLegis_definition_elements',
            i18n_domain='Avis',
        ),
        enforceVocabulary=True,
        required=True,
        vocabulary='getResponses'
    ),


    TextField(
        name='definition_elements_analyse',
        widget=TextAreaWidget(
            label=u"Analyse",
            label_msgid='Avis_label_definition_elements_analyse',
            i18n_domain='Avis',
        ),  
        default_output_type="text/html",
        default=defaultAnalysisValues['definition_elements_analyse']['yes']
    ),  


    StringField(
        name='definition_portee',
        widget=MasterSelectWidget(
            label=u"""La portée d’une définition est-elle précisée dans une autre disposition ?""",
            slave_fields = 
                     slaveFields('showIfYes', 'definition_portee_analyse'),
            label_msgid='AvisLegis_definition_portee',
            i18n_domain='Avis',
        ),
        enforceVocabulary=True,
        required=True,
        vocabulary='getResponses'
    ),


    TextField(
        name='definition_portee_analyse',
        widget=TextAreaWidget(
            label=u"Analyse",
            label_msgid='Avis_label_definition_portee_analyse',
            i18n_domain='Avis',
        ),  
        default_output_type="text/html",
        default=defaultAnalysisValues['definition_portee_analyse']['yes']
    ),  


    StringField(
        name='denomination_acte',
        widget=MasterSelectWidget(
            label=u"""La dénomination d’un acte sous une forme abrégée mentionne t-elle son type
(loi, décret, arrêté,  ) mais aussi sa date ?""",
            slave_fields = 
                     slaveFields('showIfNo', 'denomination_acte_analyse'),
            label_msgid='AvisLegis_denomination_acte',
            i18n_domain='Avis',
        ),
        enforceVocabulary=True,
        required=True,
        vocabulary='getResponsesInversed'
    ),


    TextField(
        name='denomination_acte_analyse',
        widget=TextAreaWidget(
            label=u"Analyse",
            label_msgid='Avis_label_denomination_acte_analyse',
            i18n_domain='Avis',
        ),  
        default_output_type="text/html",
        default=defaultAnalysisValues['denomination_acte_analyse']['no']
    ),  


    StringField(
        name='definitions_explicites',
        widget=MasterSelectWidget(
            label=u"""Les définitions explicitent-elles tout le texte auquel elles se
rapportent ?""",
            slave_fields = 
                     slaveFields('showIfYes', 'definitions_explicites_analyse_rassemblees') + \
                     slaveFields('showIfNo', 'definitions_explicites_analyse_un_mot'),
            label_msgid='AvisLegis_definitions_explicites',
            i18n_domain='Avis',
        ),
        enforceVocabulary=True,
        required=True,
        vocabulary='getResponses'
    ),


    StringField(
        name='definitions_explicites_analyse_rassemblees',
        widget=MasterSelectWidget(
            label=u"""Sont-elles rassemblées dans un article spécifique placé au début du texte ?""",
            slave_fields = 
                     slaveFields('showIfNo', 'definitions_explicites_analyse_rassemblees_analyse'),
            label_msgid='AvisLegis_definitions_explicites_analyse_rassemblees',
            i18n_domain='Avis',
        ),
        enforceVocabulary=True,
        required=True,
        vocabulary='getResponsesInversed'
    ),


    TextField(
        name='definitions_explicites_analyse_rassemblees_analyse',
        widget=TextAreaWidget(
            label=u"Analyse",
            label_msgid='Avis_label_definitions_explicites_analyse_rassemblees_analyse',
            i18n_domain='Avis',
        ),  
        default_output_type="text/html",
        default=defaultAnalysisValues['definitions_explicites_analyse_rassemblees_analyse']['no']
    ),  


    StringField(
        name='definitions_explicites_analyse_un_mot',
        widget=MasterSelectWidget(
            label=u"""La définition « … »concerne t-elle un mot utilisé dans un seul article ?""",
            slave_fields = 
                     slaveFields('showIfYes', 'definitions_explicites_analyse_un_mot_analyse_un_article'),
            label_msgid='AvisLegis_definitions_explicites_analyse_un_mot',
            i18n_domain='Avis',
        ),
        enforceVocabulary=True,
        required=True,
        vocabulary='getResponses'
    ),


    StringField(
        name='definitions_explicites_analyse_un_mot_analyse_un_article',
        widget=MasterSelectWidget(
            label=u"""Est-elle énoncée dans un seul article ?""",
            slave_fields = 
                     slaveFields('showIfNo', 'definitions_explicites_analyse_un_mot_analyse_un_article_analyse'),
            label_msgid='AvisLegis_definitions_explicites_analyse_un_mot_analyse_un_article',
            i18n_domain='Avis',
        ),
        enforceVocabulary=True,
        required=True,
        vocabulary='getResponsesInversed'
    ),


    TextField(
        name='definitions_explicites_analyse_un_mot_analyse_un_article_analyse',
        widget=TextAreaWidget(
            label=u"Analyse",
            label_msgid='Avis_label_definitions_explicites_analyse_un_mot_analyse_un_article_analyse',
            i18n_domain='Avis',
        ),  
        default_output_type="text/html",
        default=defaultAnalysisValues['definitions_explicites_analyse_un_mot_analyse_un_article_analyse']['no']
    ),  



    StringField(
        name='defs_nombreuses_modifs',
        widget=MasterSelectWidget(
            label=u"""Le texte initial a-t-il déjà fait l’objet de nombreuses modifications ?""",
            slave_fields = 
                     slaveFields('showIfYes', 'defs_nombreuses_modifs_analyse'),
            label_msgid='AvisLegis_defs_nombreuses_modifs',
            i18n_domain='Avis',
        ),
        enforceVocabulary=True,
        required=True,
        vocabulary='getResponses'
    ),


    TextField(
        name='defs_nombreuses_modifs_analyse',
        widget=TextAreaWidget(
            label=u"Analyse",
            label_msgid='Avis_label_nombreuses_modifs_analyse',
            i18n_domain='Avis',
        ),  
        default_output_type="text/html",
        default=defaultAnalysisValues['nombreuses_modifs_analyse']['yes']
    ),  


    StringField(
        name='modifs_excessif',
        widget=MasterSelectWidget(
            label=u"""Certains articles proposent-ils un nombre excessif de modifications ?""",
            slave_fields = 
                     slaveFields('showIfYes', 'modifs_excessif_analyse'),
            label_msgid='AvisLegis_modifs_excessif',
            i18n_domain='Avis',
        ),
        enforceVocabulary=True,
        required=True,
        vocabulary='getResponses'
    ),


    TextField(
        name='modifs_excessif_analyse',
        widget=TextAreaWidget(
            label=u"Analyse",
            label_msgid='Avis_label_modifs_excessif_analyse',
            i18n_domain='Avis',
        ),  
        default_output_type="text/html",
        default=defaultAnalysisValues['modifs_excessif_analyse']['yes']
    ),  


    StringField(
        name='modifs_estethiques',
        widget=MasterSelectWidget(
            label=u"""Y a-t-il des modifications purement esthétiques ?""",
            slave_fields = 
                     slaveFields('showIfYes', 'modifs_estethiques_analyse'),
            label_msgid='AvisLegis_modifs_estethiques',
            i18n_domain='Avis',
        ),
        enforceVocabulary=True,
        required=True,
        vocabulary='getResponses'
    ),


    TextField(
        name='modifs_estethiques_analyse',
        widget=TextAreaWidget(
            label=u"Analyse",
            label_msgid='Avis_label_modifs_estethiques_analyse',
            i18n_domain='Avis',
        ),  
        default_output_type="text/html",
        default=defaultAnalysisValues['modifs_estethiques_analyse']['yes']
    ),  


    StringField(
        name='modifs_arrete',
        widget=MasterSelectWidget(
            label=u"""S’il  s’agit d’un projet d’arrêté,  modifie-t-il ou abroge-t-il un
décret/une loi ?""",
            slave_fields = 
                     slaveFields('showIfYes', 'modifs_arrete_analyse'),
            label_msgid='AvisLegis_modifs_arrete',
            i18n_domain='Avis',
        ),
        enforceVocabulary=True,
        required=True,
        vocabulary='getResponses'
    ),


    TextField(
        name='modifs_arrete_analyse',
        widget=TextAreaWidget(
            label=u"Analyse",
            label_msgid='Avis_label_modifs_arrete_analyse',
            i18n_domain='Avis',
        ),  
        default_output_type="text/html",
        default=defaultAnalysisValues['modifs_arrete_analyse']['yes']
    ),  


    StringField(
        name='modifs_decret',
        widget=MasterSelectWidget(
            label=u"""S’il s’agit d’un projet de décret, modifie-t-il un arrêté ?""",
            slave_fields = 
                     slaveFields('showIfYes', 'modifs_decret_analyse'),
            label_msgid='AvisLegis_modifs_decret',
            i18n_domain='Avis',
        ),
        enforceVocabulary=True,
        required=True,
        vocabulary='getResponses'
    ),


    TextField(
        name='modifs_decret_analyse',
        widget=TextAreaWidget(
            label=u"Analyse",
            label_msgid='Avis_label_modifs_decret_analyse',
            i18n_domain='Avis',
        ),  
        default_output_type="text/html",
        default=defaultAnalysisValues['modifs_decret_analyse']['yes']
    ),  


    StringField(
        name='modifs_successives',
        widget=MasterSelectWidget(
            label=u"""Si le projet modifie à plusieurs reprises le même texte, les modifications
successives sont-elles bien rédigées ?""",
            slave_fields = 
                     slaveFields('showIfNo', 'modifs_successives_analyse'),
            label_msgid='AvisLegis_modifs_successives',
            i18n_domain='Avis',
        ),
        enforceVocabulary=True,
        required=True,
        vocabulary='getResponsesInversed'
    ),


    TextField(
        name='modifs_successives_analyse',
        widget=TextAreaWidget(
            label=u"Analyse",
            label_msgid='Avis_label_modifs_successives_analyse',
            i18n_domain='Avis',
        ),  
        default_output_type="text/html",
        default=defaultAnalysisValues['modifs_successives_analyse']['no']
    ),  


    StringField(
        name='modifs_historique',
        widget=MasterSelectWidget(
            label=u"""L’historique des modifications de chaque article modifié est-il mentionné ?""",
            slave_fields = 
                     slaveFields('showIfNo', 'modifs_historique_analyse'),
            label_msgid='AvisLegis_modifs_historique',
            i18n_domain='Avis',
        ),
        enforceVocabulary=True,
        required=True,
        vocabulary='getResponsesInversed'
    ),


    TextField(
        name='modifs_historique_analyse',
        widget=TextAreaWidget(
            label=u"Analyse",
            label_msgid='Avis_label_modifs_historique_analyse',
            i18n_domain='Avis',
        ),  
        default_output_type="text/html",
        default=defaultAnalysisValues['modifs_historique_analyse']['no']
    ),  


    StringField(
        name='modifs_alinea',
        widget=MasterSelectWidget(
            label=u"""Est-il prévu de modifier un ou plusieurs alinéas après en avoir inséré ou
abrogé un ou plusieurs autres ?""",
            slave_fields = 
                     slaveFields('showIfYes', 'modifs_alinea_analyse'),
            label_msgid='AvisLegis_modifs_alinea',
            i18n_domain='Avis',
        ),
        enforceVocabulary=True,
        required=True,
        vocabulary='getResponses'
    ),


    TextField(
        name='modifs_alinea_analyse',
        widget=TextAreaWidget(
            label=u"Analyse",
            label_msgid='Avis_label_modifs_alinea_analyse',
            i18n_domain='Avis',
        ),  
        default_output_type="text/html",
        default=defaultAnalysisValues['modifs_alinea_analyse']['yes']
    ),  


    StringField(
        name='modifs_acte',
        widget=MasterSelectWidget(
            label=u"""Les modifications et abrogations des articles d’un même acte sont-elles
présentées dans l’ordre numérique de ces articles ?""",
            slave_fields = 
                     slaveFields('showIfNo', 'modifs_acte_analyse'),
            label_msgid='AvisLegis_modifs_acte',
            i18n_domain='Avis',
        ),
        enforceVocabulary=True,
        required=True,
        vocabulary='getResponsesInversed'
    ),


    TextField(
        name='modifs_acte_analyse',
        widget=TextAreaWidget(
            label=u"Analyse",
            label_msgid='Avis_label_modifs_acte_analyse',
            i18n_domain='Avis',
        ),  
        default_output_type="text/html",
        default=defaultAnalysisValues['modifs_acte_analyse']['no']
    ),  


    StringField(
        name='numerotation_divisions',
        widget=MasterSelectWidget(
            label=u"""La numérotation des divisions, des articles et des paragraphes insérés est-
elle cohérente par rapport à la numérotation de base ?  """,
            slave_fields = 
                     slaveFields('showIfNo', 'numerotation_divisions_analyse'),
            label_msgid='AvisLegis_numerotation_divisions',
            i18n_domain='Avis',
        ),
        enforceVocabulary=True,
        required=True,
        vocabulary='getResponsesInversed'
    ),


    TextField(
        name='numerotation_divisions_analyse',
        widget=TextAreaWidget(
            label=u"Analyse",
            label_msgid='Avis_label_numerotation_divisions_analyse',
            i18n_domain='Avis',
        ),  
        default_output_type="text/html",
        default=defaultAnalysisValues['numerotation_divisions_analyse']['no']
    ),  


    StringField(
        name='renumerotation',
        widget=MasterSelectWidget(
            label=u"""Y a-t-il renumérotation des articles ou de toute autre division du
dispositif ?""",
            slave_fields = 
                     slaveFields('showIfYes', 'renumerotation_analyse'),
            label_msgid='AvisLegis_renumerotation',
            i18n_domain='Avis',
        ),
        enforceVocabulary=True,
        required=True,
        vocabulary='getResponses'
    ),


    TextField(
        name='renumerotation_analyse',
        widget=TextAreaWidget(
            label=u"Analyse",
            label_msgid='Avis_label_renumerotation_analyse',
            i18n_domain='Avis',
        ),  
        default_output_type="text/html",
        default=defaultAnalysisValues['renumerotation_analyse']['yes']
    ),  


    StringField(
        name='modifs_plusieurs_textes',
        widget=MasterSelectWidget(
            label=u"""Le projet modifie-t-il plusieurs textes ?""",
            slave_fields = 
                     slaveFields('showIfYes', 'modifs_plusieurs_textes_analyse'),
            label_msgid='AvisLegis_modifs_plusieurs_textes',
            i18n_domain='Avis',
        ),
        enforceVocabulary=True,
        required=True,
        vocabulary='getResponses'
    ),


    TextField(
        name='modifs_plusieurs_textes_analyse',
        widget=TextAreaWidget(
            label=u"Analyse",
            label_msgid='Avis_label_modifs_plusieurs_textes_analyse',
            i18n_domain='Avis',
        ),  
        default_output_type="text/html",
        default=defaultAnalysisValues['modifs_plusieurs_textes_analyse']['yes']
    ),  


    StringField(
        name='nombreuses_modifs',
        widget=MasterSelectWidget(
            label=u"""Les modifications sont-elles nombreuses ?""",
            slave_fields = 
                     slaveFields('showIfYes', 'nombreuses_modifs_analyse'),
            label_msgid='AvisLegis_nombreuses_modifs',
            i18n_domain='Avis',
        ),
        enforceVocabulary=True,
        required=True,
        vocabulary='getResponses'
    ),


    TextField(
        name='nombreuses_modifs_analyse',
        widget=TextAreaWidget(
            label=u"Analyse",
            label_msgid='Avis_label_nombreuses_modifs_analyse',
            i18n_domain='Avis',
        ),  
        default_output_type="text/html",
        default=defaultAnalysisValues['nombreuses_modifs_analyse']['yes']
    ),  


    StringField(
        name='modifs_actes_legislatifs',
        widget=MasterSelectWidget(
            label=u"""Modifiez–vous des actes législatifs et abrogez-vous des arrêtés dans un
même texte ?""",
            slave_fields = 
                     slaveFields('showIfYes', 'modifs_actes_legislatifs_analyse_d_abord'),
            label_msgid='AvisLegis_modifs_actes_legislatifs',
            i18n_domain='Avis',
        ),
        enforceVocabulary=True,
        required=True,
        vocabulary='getResponses'
    ),


    StringField(
        name='modifs_actes_legislatifs_analyse_d_abord',
        widget=MasterSelectWidget(
            label=u"""Traitez-vous d’abord tous les actes législatifs ?""",
            slave_fields = 
                     slaveFields('showIfNo', 'modifs_actes_legislatifs_analyse_d_abord_analyse'),
            label_msgid='AvisLegis_modifs_actes_legislatifs_analyse_d_abord',
            i18n_domain='Avis',
        ),
        enforceVocabulary=True,
        required=True,
        vocabulary='getResponsesInversed'
    ),


    TextField(
        name='modifs_actes_legislatifs_analyse_d_abord_analyse',
        widget=TextAreaWidget(
            label=u"Analyse",
            label_msgid='Avis_label_modifs_actes_legislatifs_analyse_d_abord_analyse',
            i18n_domain='Avis',
        ),  
        default_output_type="text/html",
        default=defaultAnalysisValues['modifs_actes_legislatifs_analyse_d_abord_analyse']['no']
    ),  


    StringField(
        name='modif_disposition_modif',
        widget=MasterSelectWidget(
            label=u"""Le projet modifie-t-il ou abroge-t-il une disposition modificative?""",
            slave_fields = 
                     slaveFields('showIfYes', 'modif_disposition_modif_analyse_en_vigueur') + \
                     slaveFields('showIfYes', 'modif_disposition_modif_analyse_en_vigueur_analyse'),
            label_msgid='AvisLegis_modif_disposition_modif',
            i18n_domain='Avis',
        ),
        enforceVocabulary=True,
        required=True,
        vocabulary='getResponses'
    ),


    StringField(
        name='modif_disposition_modif_analyse_en_vigueur',
        widget=MasterSelectWidget(
            label=u"""La disposition modificative est-elle entrée en vigueur ?""",
            slave_fields = 
                     slaveFields('setValue', 'modif_disposition_modif_analyse_en_vigueur_analyse'),
            label_msgid='AvisLegis_modif_disposition_modif_analyse_en_vigueur',
            i18n_domain='Avis',
        ),
        enforceVocabulary=True,
        required=True,
        vocabulary='getResponses'
    ),


    TextField(
        name='modif_disposition_modif_analyse_en_vigueur_analyse',
        widget=TextAreaWidget(
            label=u"Analyse",
            label_msgid='Avis_label_modif_disposition_modif_analyse_en_vigueur_analyse',
            i18n_domain='Avis',
        ),  
        default_output_type="text/html",
        default=defaultAnalysisValues['modif_disposition_modif_analyse_en_vigueur_analyse']['yes']
    ),  

    StringField(
        name='abroges_identifies',
        widget=MasterSelectWidget(
            label=u"""Tous les actes abrogés sont-ils identifiés ?""",
            slave_fields = 
                     slaveFields('showIfNo', 'abroges_identifies_analyse'),
            label_msgid='AvisLegis_abroges_identifies',
            i18n_domain='Avis',
        ),
        enforceVocabulary=True,
        required=True,
        vocabulary='getResponsesInversed'
    ),


    TextField(
        name='abroges_identifies_analyse',
        widget=TextAreaWidget(
            label=u"Analyse",
            label_msgid='Avis_label_abroges_identifies_analyse',
            i18n_domain='Avis',
        ),  
        default_output_type="text/html",
        default=defaultAnalysisValues['abroges_identifies_analyse']['no']
    ),  


    StringField(
        name='abroges_cites',
        widget=MasterSelectWidget(
            label=u"""Les actes abrogés sont-ils correctement cités?""",
            slave_fields = 
                     slaveFields('showIfNo', 'abroges_cites_analyse'),
            label_msgid='AvisLegis_abroges_cites',
            i18n_domain='Avis',
        ),
        enforceVocabulary=True,
        required=True,
        vocabulary='getResponsesInversed'
    ),


    TextField(
        name='abroges_cites_analyse',
        widget=TextAreaWidget(
            label=u"Analyse",
            label_msgid='Avis_label_abroges_cites_analyse',
            i18n_domain='Avis',
        ),  
        default_output_type="text/html",
        default=defaultAnalysisValues['abroges_cites_analyse']['no']
    ),  


    StringField(
        name='abroges_pertinentes',
        widget=MasterSelectWidget(
            label=u"""Les dispositions abrogatoires précisent-elles toutes les modifications
encore pertinentes ?""",
            slave_fields = 
                     slaveFields('showIfNo', 'abroges_pertinentes_analyse'),
            label_msgid='AvisLegis_abroges_pertinentes',
            i18n_domain='Avis',
        ),
        enforceVocabulary=True,
        required=True,
        vocabulary='getResponsesInversed'
    ),


    TextField(
        name='abroges_pertinentes_analyse',
        widget=TextAreaWidget(
            label=u"Analyse",
            label_msgid='Avis_label_abroges_pertinentes_analyse',
            i18n_domain='Avis',
        ),  
        default_output_type="text/html",
        default=defaultAnalysisValues['abroges_pertinentes_analyse']['no']
    ),  


    StringField(
        name='abroges_region',
        widget=MasterSelectWidget(
            label=u"""La disposition abrogatoire comporte-t-elle les mots « pour la Région
wallonne » ?""",
            slave_fields = 
                     slaveFields('showIfYes', 'abroges_region_analyse'),
            label_msgid='AvisLegis_abroges_region',
            i18n_domain='Avis',
        ),
        enforceVocabulary=True,
        required=True,
        vocabulary='getResponses'
    ),


    TextField(
        name='abroges_region_analyse',
        widget=TextAreaWidget(
            label=u"Analyse",
            label_msgid='Avis_label_abroges_region_analyse',
            i18n_domain='Avis',
        ),  
        default_output_type="text/html",
        default=defaultAnalysisValues['abroges_region_analyse']['yes']
    ),  


    StringField(
        name='abroge_finales',
        widget=MasterSelectWidget(
            label=u"""La disposition est-elle placée parmi les dispositions finales, après les
dispositions modificatives et avant les dispositions transitoires, l’entrée
en vigueur et l’article d’exécution (exécutoire) ?""",
            slave_fields = 
                     slaveFields('showIfNo', 'abroge_finales_analyse'),
            label_msgid='AvisLegis_abroge_finales',
            i18n_domain='Avis',
        ),
        enforceVocabulary=True,
        required=True,
        vocabulary='getResponsesInversed'
    ),


    TextField(
        name='abroge_finales_analyse',
        widget=TextAreaWidget(
            label=u"Analyse",
            label_msgid='Avis_label_abroge_finales_analyse',
            i18n_domain='Avis',
        ),  
        default_output_type="text/html",
        default=defaultAnalysisValues['abroge_finales_analyse']['no']
    ),  


    StringField(
        name='abroge_decret',
        widget=MasterSelectWidget(
            label=u"""S’il s’agit d’un décret :les  arrêtés d’exécution sont-ils abrogés ?""",
            slave_fields = 
                     slaveFields('showIfNo', 'abroge_decret_analyse'),
            label_msgid='AvisLegis_abroge_decret',
            i18n_domain='Avis',
        ),
        enforceVocabulary=True,
        required=True,
        vocabulary='getResponsesInversed'
    ),


    TextField(
        name='abroge_decret_analyse',
        widget=TextAreaWidget(
            label=u"Analyse",
            label_msgid='Avis_label_abroge_decret_analyse',
            i18n_domain='Avis',
        ),  
        default_output_type="text/html",
        default=defaultAnalysisValues['abroge_decret_analyse']['no']
    ),  



    StringField(
        name='transitoires',
        widget=MasterSelectWidget(
            label=u"""Le projet contient-il des dispositions transitoires?""",
            slave_fields = 
                     slaveFields('showIfYes', 'transitoires_analyse_permanent', 'transitoires_analyse_temporaire'),
            label_msgid='AvisLegis_transitoires',
            i18n_domain='Avis',
        ),
        enforceVocabulary=True,
        required=True,
        vocabulary='getResponses'
    ),


    StringField(
        name='transitoires_analyse_permanent',
        widget=MasterSelectWidget(
            label=u"""S'agit-il de dispositions qui ont un caractère permanent?""",
            slave_fields = 
                     slaveFields('showIfYes', 'transitoires_analyse_permanent_analyse'),
            label_msgid='AvisLegis_transitoires_analyse_permanent',
            i18n_domain='Avis',
        ),
        enforceVocabulary=True,
        required=True,
        vocabulary='getResponses'
    ),


    TextField(
        name='transitoires_analyse_permanent_analyse',
        widget=TextAreaWidget(
            label=u"Analyse",
            label_msgid='Avis_label_transitoires_analyse_permanent_analyse',
            i18n_domain='Avis',
        ),  
        default_output_type="text/html",
        default=defaultAnalysisValues['transitoires_analyse_permanent_analyse']['yes']
    ),  


    StringField(
        name='transitoires_analyse_temporaire',
        widget=MasterSelectWidget(
            label=u"""S'agit-il de dispositions temporaires?""",
            slave_fields = 
                     slaveFields('showIfYes', 'transitoires_analyse_temporaire_analyse'),
            label_msgid='AvisLegis_transitoires_analyse_temporaire',
            i18n_domain='Avis',
        ),
        enforceVocabulary=True,
        required=True,
        vocabulary='getResponses'
    ),


    TextField(
        name='transitoires_analyse_temporaire_analyse',
        widget=TextAreaWidget(
            label=u"Analyse",
            label_msgid='Avis_label_transitoires_analyse_temporaire_analyse',
            i18n_domain='Avis',
        ),  
        default_output_type="text/html",
        default=defaultAnalysisValues['transitoires_analyse_temporaire_analyse']['yes']
    ),  



    StringField(
        name='entree',
        widget=MasterSelectWidget(
            label=u"""Le texte prévoit-il une date d’entrée en vigueur ?""",
            slave_fields = 
                     slaveFields('showIfNo', 'entree_analyse') + \
                     slaveFields('showIfYes', 'entree_analyse_delai', 'entree_analyse_retroactive', 'entree_analyse_informe', 'entree_analyse_temps', 'entree_analyse_mesures_materielles', 'entree_analyse_date_entree', 'entree_analyse_preambule', 'entree_analyse_a_la_fin'),
            label_msgid='AvisLegis_entree',
            i18n_domain='Avis',
        ),
        enforceVocabulary=True,
        required=True,
        vocabulary='getResponsesInversed'
    ),


    TextField(
        name='entree_analyse',
        widget=TextAreaWidget(
            label=u"Analyse",
            label_msgid='Avis_label_entree_analyse',
            i18n_domain='Avis',
        ),  
        default_output_type="text/html",
        default=defaultAnalysisValues['entree_analyse']['no']
    ),  


    StringField(
        name='entree_analyse_delai',
        widget=MasterSelectWidget(
            label=u"""Existe t-il des raisons impérieuses de déroger au délai de
10 jours?""",
            slave_fields = 
                     slaveFields('showIfNo', 'entree_analyse_delai_analyse'),
            label_msgid='AvisLegis_entree_analyse_delai',
            i18n_domain='Avis',
        ),
        enforceVocabulary=True,
        required=True,
        vocabulary='getResponsesInversed'
    ),


    TextField(
        name='entree_analyse_delai_analyse',
        widget=TextAreaWidget(
            label=u"Analyse",
            label_msgid='Avis_label_entree_analyse_delai_analyse',
            i18n_domain='Avis',
        ),  
        default_output_type="text/html",
        default=defaultAnalysisValues['entree_analyse_delai_analyse']['no']
    ),  


    StringField(
        name='entree_analyse_retroactive',
        widget=MasterSelectWidget(
            label=u"""La date d’entrée en vigueur est-elle rétroactive ?""",
            slave_fields = 
                     slaveFields('showIfYes', 'entree_analyse_retroactive_analyse'),
            label_msgid='AvisLegis_entree_analyse_retroactive',
            i18n_domain='Avis',
        ),
        enforceVocabulary=True,
        required=True,
        vocabulary='getResponses'
    ),


    TextField(
        name='entree_analyse_retroactive_analyse',
        widget=TextAreaWidget(
            label=u"Analyse",
            label_msgid='Avis_label_entree_analyse_retroactive_analyse',
            i18n_domain='Avis',
        ),  
        default_output_type="text/html",
        default=defaultAnalysisValues['entree_analyse_retroactive_analyse']['yes']
    ),  


    StringField(
        name='entree_analyse_informe',
        widget=MasterSelectWidget(
            label=u"""Est-il nécessaire d’informer les personnes concernées par
les règles nouvelles afin qu’elles puissent les appliquer facilement et
correctement ?""",
            slave_fields = 
                     slaveFields('showIfYes', 'entree_analyse_informe_analyse'),
            label_msgid='AvisLegis_entree_analyse_informe',
            i18n_domain='Avis',
        ),
        enforceVocabulary=True,
        required=True,
        vocabulary='getResponses'
    ),


    TextField(
        name='entree_analyse_informe_analyse',
        widget=TextAreaWidget(
            label=u"Analyse",
            label_msgid='Avis_label_entree_analyse_informe_analyse',
            i18n_domain='Avis',
        ),  
        default_output_type="text/html",
        default=defaultAnalysisValues['entree_analyse_informe_analyse']['yes']
    ),  


    StringField(
        name='entree_analyse_temps',
        widget=MasterSelectWidget(
            label=u"""Les personnes tenues d’obligations nouvelles auront-
elles besoin de temps pour se conformer à ces obligations ?""",
            slave_fields = 
                     slaveFields('showIfYes', 'entree_analyse_temps_analyse'),
            label_msgid='AvisLegis_entree_analyse_temps',
            i18n_domain='Avis',
        ),
        enforceVocabulary=True,
        required=True,
        vocabulary='getResponses'
    ),


    TextField(
        name='entree_analyse_temps_analyse',
        widget=TextAreaWidget(
            label=u"Analyse",
            label_msgid='Avis_label_entree_analyse_temps_analyse',
            i18n_domain='Avis',
        ),  
        default_output_type="text/html",
        default=defaultAnalysisValues['entree_analyse_temps_analyse']['yes']
    ),  


    StringField(
        name='entree_analyse_mesures_materielles',
        widget=MasterSelectWidget(
            label=u"""Les services chargés de l’application de l’acte
doivent-ils prendre préalablement des mesures matérielles d’exécution
telles que l’achat de matériel, l’engagement de personnel, sa formation, la
mise au point de programmes informatiques ?""",
            slave_fields = 
                     slaveFields('showIfYes', 'entree_analyse_mesures_materielles_analyse'),
            label_msgid='AvisLegis_entree_analyse_mesures_materielles',
            i18n_domain='Avis',
        ),
        enforceVocabulary=True,
        required=True,
        vocabulary='getResponses'
    ),


    TextField(
        name='entree_analyse_mesures_materielles_analyse',
        widget=TextAreaWidget(
            label=u"Analyse",
            label_msgid='Avis_label_entree_analyse_mesures_materielles_analyse',
            i18n_domain='Avis',
        ),  
        default_output_type="text/html",
        default=defaultAnalysisValues['entree_analyse_mesures_materielles_analyse']['yes']
    ),  


    StringField(
        name='entree_analyse_date_entree',
        widget=MasterSelectWidget(
            label=u"""Est-il prévu que le GW fixe la date d’entrée en vigueur d'un décret?""",
            slave_fields = 
                     slaveFields('showIfYes', 'entree_analyse_date_entree_analyse_ultime'),
            label_msgid='AvisLegis_entree_analyse_date_entree',
            i18n_domain='Avis',
        ),
        enforceVocabulary=True,
        required=True,
        vocabulary='getResponses'
    ),


    StringField(
        name='entree_analyse_date_entree_analyse_ultime',
        widget=MasterSelectWidget(
            label=u"""L'habilitation législative prévoit-elle une date ultime d'entrée
en vigueur de l'acte législatif?""",
            slave_fields = 
                     slaveFields('showIfNo', 'entree_analyse_date_entree_analyse_ultime_analyse'),
            label_msgid='AvisLegis_entree_analyse_date_entree_analyse_ultime',
            i18n_domain='Avis',
        ),
        enforceVocabulary=True,
        required=True,
        vocabulary='getResponsesInversed'
    ),


    TextField(
        name='entree_analyse_date_entree_analyse_ultime_analyse',
        widget=TextAreaWidget(
            label=u"Analyse",
            label_msgid='Avis_label_entree_analyse_date_entree_analyse_ultime_analyse',
            i18n_domain='Avis',
        ),  
        default_output_type="text/html",
        default=defaultAnalysisValues['entree_analyse_date_entree_analyse_ultime_analyse']['no']
    ),  


    StringField(
        name='entree_analyse_preambule',
        widget=MasterSelectWidget(
            label=u"""Le préambule de l'arrêté mettant en vigueur l'acte législatif indique-t-il
son fondement juridique?""",
            slave_fields = 
                     slaveFields('showIfNo', 'entree_analyse_preambule_analyse'),
            label_msgid='AvisLegis_entree_analyse_preambule',
            i18n_domain='Avis',
        ),
        enforceVocabulary=True,
        required=True,
        vocabulary='getResponsesInversed'
    ),


    TextField(
        name='entree_analyse_preambule_analyse',
        widget=TextAreaWidget(
            label=u"Analyse",
            label_msgid='Avis_label_entree_analyse_preambule_analyse',
            i18n_domain='Avis',
        ),  
        default_output_type="text/html",
        default=defaultAnalysisValues['entree_analyse_preambule_analyse']['no']
    ),  


    StringField(
        name='entree_analyse_a_la_fin',
        widget=MasterSelectWidget(
            label=u"""La disposition fixant l'entrée en vigueur est-elle placée à la fin de
l'acte, avant l'article d'exécution?""",
            slave_fields = 
                     slaveFields('showIfNo', 'entree_analyse_a_la_fin_analyse'),
            label_msgid='AvisLegis_entree_analyse_a_la_fin',
            i18n_domain='Avis',
        ),
        enforceVocabulary=True,
        required=True,
        vocabulary='getResponsesInversed'
    ),


    TextField(
        name='entree_analyse_a_la_fin_analyse',
        widget=TextAreaWidget(
            label=u"Analyse",
            label_msgid='Avis_label_entree_analyse_a_la_fin_analyse',
            i18n_domain='Avis',
        ),  
        default_output_type="text/html",
        default=defaultAnalysisValues['entree_analyse_a_la_fin_analyse']['no']
    ),  



    StringField(
        name='ministre_designe',
        widget=MasterSelectWidget(
            label=u"""Le projet désigne-t-il un ministre individuellement en citant ses
attributions telles qu'elles figurent dans son arrêté de nomination ?""",
            slave_fields = 
                     slaveFields('showIfYes', 'ministre_designe_analyse'),
            label_msgid='AvisLegis_ministre_designe',
            i18n_domain='Avis',
        ),
        enforceVocabulary=True,
        required=True,
        vocabulary='getResponses'
    ),


    TextField(
        name='ministre_designe_analyse',
        widget=TextAreaWidget(
            label=u"Analyse",
            label_msgid='Avis_label_ministre_designe_analyse',
            i18n_domain='Avis',
        ),  
        default_output_type="text/html",
        default=defaultAnalysisValues['ministre_designe_analyse']['yes']
    ),  



    StringField(
        name='annexe',
        widget=MasterSelectWidget(
            label=u"""L'annexe contient-elle dans son en-tête le mot "annexe"?""",
            slave_fields = 
                     slaveFields('showIfNo', 'annexe_analyse'),
            label_msgid='AvisLegis_annexe',
            i18n_domain='Avis',
        ),
        enforceVocabulary=True,
        required=True,
        vocabulary='getResponsesInversed'
    ),


    TextField(
        name='annexe_analyse',
        widget=TextAreaWidget(
            label=u"Analyse",
            label_msgid='Avis_label_annexe_analyse',
            i18n_domain='Avis',
        ),  
        default_output_type="text/html",
        default=defaultAnalysisValues['annexe_analyse']['no']
    ),  


    StringField(
        name='annexe_intitule',
        widget=MasterSelectWidget(
            label=u"""L'annexe a-t-elle un intitulé précis, complet et concis ?""",
            slave_fields = 
                     slaveFields('showIfNo', 'annexe_intitule_analyse'),
            label_msgid='AvisLegis_annexe_intitule',
            i18n_domain='Avis',
        ),
        enforceVocabulary=True,
        required=True,
        vocabulary='getResponsesInversed'
    ),


    TextField(
        name='annexe_intitule_analyse',
        widget=TextAreaWidget(
            label=u"Analyse",
            label_msgid='Avis_label_annexe_intitule_analyse',
            i18n_domain='Avis',
        ),  
        default_output_type="text/html",
        default=defaultAnalysisValues['annexe_intitule_analyse']['no']
    ),  


    StringField(
        name='annexe_droits',
        widget=MasterSelectWidget(
            label=u"""L'annexe énonce-t-elle des droits et obligations autres que ceux mentionnés
dans le dispositif ?""",
            slave_fields = 
                     slaveFields('showIfYes', 'annexe_droits_analyse'),
            label_msgid='AvisLegis_annexe_droits',
            i18n_domain='Avis',
        ),
        enforceVocabulary=True,
        required=True,
        vocabulary='getResponses'
    ),


    TextField(
        name='annexe_droits_analyse',
        widget=TextAreaWidget(
            label=u"Analyse",
            label_msgid='Avis_label_annexe_droits_analyse',
            i18n_domain='Avis',
        ),  
        default_output_type="text/html",
        default=defaultAnalysisValues['annexe_droits_analyse']['yes']
    ),  


    StringField(
        name='annexe_mention',
        widget=MasterSelectWidget(
            label=u"""L'annexe du projet mentionne-t-elle la formule "Vu pour être annexé à
l'arrêté du Gouvernement du (date et intitulé) ?""",
            slave_fields = 
                     slaveFields('showIfNo', 'annexe_mention_analyse'),
            label_msgid='AvisLegis_annexe_mention',
            i18n_domain='Avis',
        ),
        enforceVocabulary=True,
        required=True,
        vocabulary='getResponsesInversed'
    ),


    TextField(
        name='annexe_mention_analyse',
        widget=TextAreaWidget(
            label=u"Analyse",
            label_msgid='Avis_label_annexe_mention_analyse',
            i18n_domain='Avis',
        ),  
        default_output_type="text/html",
        default=defaultAnalysisValues['annexe_mention_analyse']['no']
    ),  


    StringField(
        name='annexe_signatures',
        widget=MasterSelectWidget(
            label=u"""L'annexe du projet est-elle soumise aux mêmes signatures que le texte
auquel elle est jointe ?""",
            slave_fields = 
                     slaveFields('showIfNo', 'annexe_signatures_analyse'),
            label_msgid='AvisLegis_annexe_signatures',
            i18n_domain='Avis',
        ),
        enforceVocabulary=True,
        required=True,
        vocabulary='getResponsesInversed'
    ),


    TextField(
        name='annexe_signatures_analyse',
        widget=TextAreaWidget(
            label=u"Analyse",
            label_msgid='Avis_label_annexe_signatures_analyse',
            i18n_domain='Avis',
        ),  
        default_output_type="text/html",
        default=defaultAnalysisValues['annexe_signatures_analyse']['no']
    ),  



    StringField(
        name='phrases_simples',
        widget=MasterSelectWidget(
            label=u"""Les phrases sont-elles simples, claires, précises, concises et cohérentes ?""",
            slave_fields = 
                     slaveFields('showIfNo', 'phrases_simples_analyse'),
            label_msgid='AvisLegis_phrases_simples',
            i18n_domain='Avis',
        ),
        enforceVocabulary=True,
        required=True,
        vocabulary='getResponsesInversed'
    ),


    TextField(
        name='phrases_simples_analyse',
        widget=TextAreaWidget(
            label=u"Analyse",
            label_msgid='Avis_label_phrases_simples_analyse',
            i18n_domain='Avis',
        ),  
        default_output_type="text/html",
        default=defaultAnalysisValues['phrases_simples_analyse']['no']
    ),  


    StringField(
        name='repartition_competences',
        widget=MasterSelectWidget(
            label=u"""Y a-t-il un problème en matière de répartition des compétences ?""",
            slave_fields = 
                     slaveFields('showIfYes', 'repartition_competences_analyse'),
            label_msgid='AvisLegis_repartition_competences',
            i18n_domain='Avis',
        ),
        enforceVocabulary=True,
        required=True,
        vocabulary='getResponses'
    ),


    TextField(
        name='repartition_competences_analyse',
        widget=TextAreaWidget(
            label=u"Analyse",
            label_msgid='Avis_label_repartition_competences_analyse',
            i18n_domain='Avis',
        ),  
        default_output_type="text/html",
        default=defaultAnalysisValues['repartition_competences_analyse']['yes']
    ),  


    StringField(
        name='libertes_publiques',
        widget=MasterSelectWidget(
            label=u"""Y a-t-il un problème en matière de libertés publiques ?""",
            slave_fields = 
                     slaveFields('showIfYes', 'libertes_publiques_analyse'),
            label_msgid='AvisLegis_libertes_publiques',
            i18n_domain='Avis',
        ),
        enforceVocabulary=True,
        required=True,
        vocabulary='getResponses'
    ),


    TextField(
        name='libertes_publiques_analyse',
        widget=TextAreaWidget(
            label=u"Analyse",
            label_msgid='Avis_label_libertes_publiques_analyse',
            i18n_domain='Avis',
        ),  
        default_output_type="text/html",
        default=defaultAnalysisValues['libertes_publiques_analyse']['yes']
    ),  


    StringField(
        name='droit_europeen',
        widget=MasterSelectWidget(
            label=u"""Y a-t-il un problème par rapport au droit européen ? Par rapport au décret
contre la discrimination ?Par rapport à la transparence de
l’administration ?""",
            slave_fields = 
                     slaveFields('showIfYes', 'droit_europeen_analyse'),
            label_msgid='AvisLegis_droit_europeen',
            i18n_domain='Avis',
        ),
        enforceVocabulary=True,
        required=True,
        vocabulary='getResponses'
    ),


    TextField(
        name='droit_europeen_analyse',
        widget=TextAreaWidget(
            label=u"Analyse",
            label_msgid='Avis_label_droit_europeen_analyse',
            i18n_domain='Avis',
        ),  
        default_output_type="text/html",
        default=defaultAnalysisValues['droit_europeen_analyse']['yes']
    ),  

),)

MyBaseSchema = BaseSchema.copy()
AvisLegis_schema = MyBaseSchema.copy() + schema.copy()

class AvisLegis(BaseContent, AvisOdt):
    '''Critique légistique d'un texte'''
    security = ClassSecurityInfo()
    __implements__ = (getattr(BaseContent,'__implements__',()),)

    # This name appears in the 'add' box
    archetype_name = u'Avis légistique'
    meta_type = 'AvisLegis'
    portal_type = 'AvisLegis'
    allowed_content_types = []
    filter_content_types = 0
    global_allow = False
    #content_icon = 'Avis.gif'
    immediate_view = 'base_view'
    default_view = 'base_view'
    suppl_views = ()
    typeDescription = "AvisLegis"
    typeDescMsgId = 'description_edit_avis_simplif'

    actions =  (
       {'action': "string:$object_url/generateOdt",
        'category': "document_actions",
        'id': 'asOdt',
        'name': u'Générer en Open Document Format',
        'permissions': ("View",),
        'condition': 'python:1'
       },
       {'action': "string:$object_url/generateDoc",
        'category': "document_actions",
        'id': 'asDoc',
        'name': u'Générer au format Microsoft Word',
        'permissions': ("View",),
        'condition': 'python:1'
       },
    )

    _at_rename_after_creation = True
    schema = AvisLegis_schema

    apPrefixes = {'arreteGouv': "l'",
                  'arreteMin': "l'",
                  'decret': "le "
                 }
    apdPrefixes = {'arreteGouv': "d'",
                  'arreteMin': "d'",
                  'decret': "de "
                 }

    security.declarePublic('generateOdt')
    def generateOdt(self, RESPONSE):
        '''Generates the ODT version of this advice.'''
        return self._generate(RESPONSE, 'odt')

    security.declarePublic('generateDoc')
    def generateDoc(self, RESPONSE):
        '''Generates the Doc version of this advice.'''
        return self._generate(RESPONSE, 'doc')

    security.declarePublic('getResponses')
    def getResponses(self):
        """Returns the predefined responses that are possible for each question.
        """
        return DisplayList( (('no', 'Non'), ('yes', 'Oui')) )

    security.declarePublic('getResponsesInversed')
    def getResponsesInversed(self):
        """Returns the predefined responses that are possible for each question.
        """
        return DisplayList( (('yes', 'Oui'), ('no', 'Non')) )

    def isEmpty(self, fieldName):
        '''Is the rich text field p_fieldName empty?'''
        accessor = 'get%s%s' % (fieldName[0].upper(), fieldName[1:])
        fieldContent = getattr(self, accessor)()
        return fieldContent.strip() == ''

    def getUserName(self):
        userInfo = self.portal_membership.getMemberById(self.Creator())
        return userInfo.getProperty('fullname').decode('latin-1')

    def getUserEmail(self):
        return '%s@easi.wallonie.be' % self.Creator()

    def defounique_analyse_nombres_analyse_tres_eleve_(self, unique_analyse_nombres_analyse_tres_eleve_CP):
        return defaultAnalysisValues['unique_analyse_nombres_analyse_tres_eleve_analyse'][unique_analyse_nombres_analyse_tres_eleve_CP]

    def defomodif_disposition_modif_analyse_en_vigueur_(self, modif_disposition_modif_analyse_en_vigueur_CP):
        return defaultAnalysisValues['modif_disposition_modif_analyse_en_vigueur_analyse'][modif_disposition_modif_analyse_en_vigueur_CP]

    def get_decretPresentationAnalyse(self, param):
        if param == u'Oui'.encode('utf-8'):
            return ''
        if param == 'Non':
            return defaultAnalysisValues['decret_analyse_presentation_analyse'].get('no')
        return defaultAnalysisValues['decret_analyse_presentation_analyse_bien_redige_analyse'].get('no')

    def hasFieldDefaultValue(self, field):
        v = getattr(self, field)
        if v is None:
            return True
        if self.getField(field).vocabulary == 'getResponses' and v == 'no':
            return True
        if self.getField(field).vocabulary == 'getResponsesInversed' and v == 'yes':
            return True
        return False

    def has_section_questions_prealables(self):
        return \
            not(self.hasFieldDefaultValue('directive')) or \
            not(self.hasFieldDefaultValue('directive_analyse_article1')) or \
            not(self.hasFieldDefaultValue('directive_analyse_alteration')) or \
            not(self.hasFieldDefaultValue('directive_analyse_reference')) or \
            not(self.hasFieldDefaultValue('directive_analyse_litteralement')) or \
            not(self.hasFieldDefaultValue('directive_analyse_particuliers')) or \
            not(self.hasFieldDefaultValue('directive_analyse_regionales')) or \
            not(self.hasFieldDefaultValue('directive_analyse_circulaire')) or \
            not(self.hasFieldDefaultValue('traite')) or \
            not(self.hasFieldDefaultValue('traite_analyse_constitution')) or \
            not(self.hasFieldDefaultValue('traite_analyse_plusieurs')) or \
            not(self.hasFieldDefaultValue('traite_analyse_plusieurs_analyse_distincts')) or \
            not(self.hasFieldDefaultValue('cooperation')) or \
            not(self.hasFieldDefaultValue('cooperation_analyse_parties'))

    def has_section_intitule(self):
        return not(self.hasFieldDefaultValue('precis'))

    def has_section_presentation(self):
        return \
            self.decret == 'yes' and \
            not(self.decret_analyse_presentation_analyse.strip())

    def has_section_preambule(self):
        return \
            not(self.hasFieldDefaultValue('debut')) or \
            not(self.hasFieldDefaultValue('reglement')) or \
            not(self.hasFieldDefaultValue('actes_fondement')) or \
            not(self.hasFieldDefaultValue('actes_modifies')) or \
            not(self.hasFieldDefaultValue('formalites')) or \
            not(self.hasFieldDefaultValue('ministres')) or \
            not(self.hasFieldDefaultValue('formalites_non_obligatoires')) or \
            not(self.hasFieldDefaultValue('justification_arrete')) or \
            not(self.hasFieldDefaultValue('mot_final')) or \
            not(self.hasFieldDefaultValue('norme_particuliere'))

    def has_section_dispositif(self):
        return \
            not(self.hasFieldDefaultValue('unique')) or \
            not(self.hasFieldDefaultValue('unique_analyse_correctement_indique')) or \
            not(self.hasFieldDefaultValue('unique_analyse_numerotation')) or \
            not(self.hasFieldDefaultValue('unique_analyse_intitule')) or \
            not(self.hasFieldDefaultValue('unique_analyse_intitule_analyse_specifique')) or \
            not(self.hasFieldDefaultValue('unique_analyse_intitule_analyse_fidele')) or \
            not(self.hasFieldDefaultValue('unique_analyse_division')) or \
            not(self.hasFieldDefaultValue('unique_analyse_numerotation_phrase')) or \
            not(self.hasFieldDefaultValue('unique_analyse_phrases_incidentes')) or \
            not(self.hasFieldDefaultValue('unique_analyse_groupes_articles')) or \
            not(self.hasFieldDefaultValue('unique_analyse_une_division')) or \
            not(self.hasFieldDefaultValue('unique_analyse_chiffres_arabes')) or \
            not(self.hasFieldDefaultValue('unique_analyse_intitule_division')) or \
            not(self.hasFieldDefaultValue('unique_analyse_references')) or \
            not(self.hasFieldDefaultValue('unique_analyse_reiteratives')) or \
            not(self.hasFieldDefaultValue('unique_analyse_sans_portee')) or \
            not(self.hasFieldDefaultValue('unique_analyse_portee_individuelle')) or \
            not(self.hasFieldDefaultValue('unique_analyse_parenthses')) or \
            not(self.hasFieldDefaultValue('unique_analyse_nombres')) or \
            not(self.hasFieldDefaultValue('unique_analyse_nombres_analyse_tres_eleve')) or \
            not(self.hasFieldDefaultValue('unique_analyse_sigle')) or \
            not(self.hasFieldDefaultValue('unique_analyse_sigle_analyse_lettres')) or \
            not(self.hasFieldDefaultValue('unique_analyse_sigle_analyse_points')) or \
            not(self.hasFieldDefaultValue('unique_analyse_euros')) or \
            not(self.hasFieldDefaultValue('unique_analyse_euros_analyse_minuscule')) or \
            not(self.hasFieldDefaultValue('unique_analyse_euros_analyse_eur')) or \
            not(self.hasFieldDefaultValue('unique_analyse_euros_analyse_sigle')) or \
            not(self.hasFieldDefaultValue('unique_analyse_majuscule')) or \
            not(self.hasFieldDefaultValue('transfert')) or \
            not(self.hasFieldDefaultValue('transfert_analyse_article1')) or \
            not(self.hasFieldDefaultValue('matiere_regionale')) or \
            not(self.hasFieldDefaultValue('definition_dictionnaire')) or \
            not(self.hasFieldDefaultValue('definition_acte')) or \
            not(self.hasFieldDefaultValue('definition_acte_superieur')) or \
            not(self.hasFieldDefaultValue('definition_elements')) or \
            not(self.hasFieldDefaultValue('definition_portee')) or \
            not(self.hasFieldDefaultValue('denomination_acte')) or \
            not(self.hasFieldDefaultValue('definitions_explicites')) or \
            not(self.hasFieldDefaultValue('definitions_explicites_analyse_rassemblees')) or \
            not(self.hasFieldDefaultValue('definitions_explicites_analyse_un_mot')) or \
            not(self.hasFieldDefaultValue('definitions_explicites_analyse_un_mot_analyse_un_article')) or \
            not(self.hasFieldDefaultValue('nombreuses_modifs')) or \
            not(self.hasFieldDefaultValue('modifs_excessif')) or \
            not(self.hasFieldDefaultValue('modifs_estethiques')) or \
            not(self.hasFieldDefaultValue('modifs_arrete')) or \
            not(self.hasFieldDefaultValue('modifs_decret')) or \
            not(self.hasFieldDefaultValue('modifs_successives')) or \
            not(self.hasFieldDefaultValue('modifs_historique')) or \
            not(self.hasFieldDefaultValue('modifs_alinea')) or \
            not(self.hasFieldDefaultValue('modifs_acte')) or \
            not(self.hasFieldDefaultValue('numerotation_divisions')) or \
            not(self.hasFieldDefaultValue('renumerotation')) or \
            not(self.hasFieldDefaultValue('modifs_plusieurs_textes')) or \
            not(self.hasFieldDefaultValue('nombreuses_modifs')) or \
            not(self.hasFieldDefaultValue('modifs_actes_legislatifs')) or \
            not(self.hasFieldDefaultValue('modifs_actes_legislatifs_analyse_d_abord')) or \
            not(self.hasFieldDefaultValue('modif_disposition_modif')) or \
            not(self.hasFieldDefaultValue('modif_disposition_modif_analyse_en_vigueur')) or \
            not(self.hasFieldDefaultValue('abroges_identifies')) or \
            not(self.hasFieldDefaultValue('abroges_cites')) or \
            not(self.hasFieldDefaultValue('abroges_pertinentes')) or \
            not(self.hasFieldDefaultValue('abroges_region')) or \
            not(self.hasFieldDefaultValue('abroge_finales')) or \
            not(self.hasFieldDefaultValue('abroge_decret')) or \
            not(self.hasFieldDefaultValue('transitoires')) or \
            not(self.hasFieldDefaultValue('transitoires_analyse_permanent')) or \
            not(self.hasFieldDefaultValue('transitoires_analyse_temporaire')) or \
            not(self.hasFieldDefaultValue('entree')) or \
            not(self.hasFieldDefaultValue('entree_analyse_delai')) or \
            not(self.hasFieldDefaultValue('entree_analyse_retroactive')) or \
            not(self.hasFieldDefaultValue('entree_analyse_informe')) or \
            not(self.hasFieldDefaultValue('entree_analyse_temps')) or \
            not(self.hasFieldDefaultValue('entree_analyse_mesures_materielles')) or \
            not(self.hasFieldDefaultValue('entree_analyse_date_entree')) or \
            not(self.hasFieldDefaultValue('entree_analyse_date_entree_analyse_ultime')) or \
            not(self.hasFieldDefaultValue('entree_analyse_preambule')) or \
            not(self.hasFieldDefaultValue('entree_analyse_a_la_fin')) or \
            not(self.hasFieldDefaultValue('ministre_designe'))

    def has_section_annexe(self):
        return \
            not(self.hasFieldDefaultValue('annexe')) or \
            not(self.hasFieldDefaultValue('annexe_intitule')) or \
            not(self.hasFieldDefaultValue('annexe_droits')) or \
            not(self.hasFieldDefaultValue('annexe_mention')) or \
            not(self.hasFieldDefaultValue('annexe_signatures'))

    def has_section_autres(self):
        return \
            not(self.hasFieldDefaultValue('phrases_simples')) or \
            not(self.hasFieldDefaultValue('repartition_competences')) or \
            not(self.hasFieldDefaultValue('libertes_publiques')) or \
            not(self.hasFieldDefaultValue('droit_europeen'))

registerType(AvisLegis, PROJECTNAME)
# end of class AvisLegis

##code-section module-footer #fill in your manual code here
##/code-section module-footer



